<!--Site Header-->
<?php include('header.php'); ?>
    <!-- Site content -->
    <div id="content" class="content-1" style="margin:0;padding:0;">
        <section id="map-section" class="inner over" style="margin:0;padding:0;">
            <div class="map-container" style="position:relative;margin:0;padding:0;">
                <!-- Map -->
                <div id="mapplic" style="margin:0;padding:0;"></div>
                <div class="map-overlay">
                    <div class="explorebtn">বিস্তারিত দেখুন</div>
                </div>
                <span class="map-close">বন্ধ করুন</span>
            </div>
        </section>
    </div>

    <!-- Site footer -->
    <?php include('footer.php'); ?>
<!--</div>-->

<!-- Scripts -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/hammer.min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="mapplic/mapplic.js"></script>
<script>
    $(".explorebtn").click(function () {
        $(".map-overlay").hide();
        $(".map-close").show();
    });
    $(".map-close").click(function () {
        $(".map-overlay").show();
        $(".map-close").hide();
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
        var mapplic = $('#mapplic').mapplic({
            //source: 'brazil.json',	// Using mall.json file as map data
            source: 'bangladeshMap.json',	// Using mall.json file as map data
            //source: 'world.json',	// Using mall.json file as map data
            sidebar: false, 			    // Enable sidebar
            minimap: false, 			    // Enable minimap
            markers: true, 		            // Disable markers
            fillcolor: true, 		        // Disable default fill color
            fullscreen: true, 		        // Enable fullscreen
            maxscale: 12 			        // Setting maxscale to 3 times bigger than the original file
        });


    });

    //=== Stop Right click Event
    $(document).on({
        "contextmenu": function(e) {
            console.log("ctx menu button:", e.which);
    
            // Stop the context menu
            e.preventDefault();
        },
        "mousedown": function(e) {
            console.log("normal mouse down:", e.which);
        },
        "mouseup": function(e) {
            console.log("normal mouse up:", e.which);
        }
    });


</script>
</body>
</html>