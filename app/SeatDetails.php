<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class SeatDetails extends Model
{

    use SoftDeletes;
    protected $table = 'seats';
    protected $fillable = ['seat_id','election_id','division_id','district_id','extent','male_voter','female_voter','total_voter','status'];
    protected $dates = ['deleted_at'];
    /**
     * boot function for created and updated by user
     *
     * */

    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
