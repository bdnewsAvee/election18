<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Seats extends Model
{
    use SoftDeletes;
    protected $table = 'seats';
    protected $fillable = ['division_id','district_id','name','description','constituency','status'];
    protected $dates = ['deleted_at'];
    public function getDivision()
    {
        return $this->belongsTo(Divisions::class, 'division_id', 'id');
    }
    public function getDistrict()
    {
        return $this->belongsTo(Districts::class, 'district_id', 'id');
    }

    public function relDivision()
    {
        return $this->belongsTo('App\Divisions', 'division_id', 'id');
    }

    /**
     * boot function for created and updated by user
     *
     * */

    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
