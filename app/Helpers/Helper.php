<?php
namespace App\Helpers;

class Helper{

    public static  function managePagination($obj)
    {
        $serial=1;
        if($obj->currentPage()>1)
        {
            $serial=(($obj->currentPage()-1)*$obj->perPage())+1;
			
        }
        return $serial;
    }


    public static function request($url, $param, $method = 'GET')
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 40);
        curl_setopt($curl, CURLOPT_TIMEOUT, 90);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
        }

        $response = curl_exec($curl);

     /*   if ($response === false) {
            $logMessage = 'Curl error: ' . curl_error($curl) . ' Error no: ' . curl_errno($curl);
            writeToLog($logMessage, 'alert');

        }*/
        curl_close($curl);

        //echo $response;exit;
        return $response;
    }
}



