<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Helper;
use App\ElectionManifesto;
use App\PoliticalParty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class IshtehaarController extends Controller
{
    protected $allishtehaarlist;

    public function __construct(ElectionManifesto $allishtehaarlist)
    {
        $this->allishtehaarlist = $allishtehaarlist;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['title'] = "ElectionManifesto Lists";
        $all = $this->allishtehaarlist;

        if ($request->has('party_name') && $request->party_name != null) {
            $all = $all->where('political_parties_id', $request->party_name);
        }

        $data['allishtehaar'] = $all->paginate(config('system.pagination.items_per_page'));
        $data['serial'] = Helper::managePagination($data['allishtehaar']);
        $data['party'] = PoliticalParty::with('partyName')->where('status', 'Active')->pluck('name', 'id');
        if ($request->ajax()) {
            return view('backend.ishtehaar.table_load', $data)->render();
        }
        return view('backend.ishtehaar.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['title'] = "ElectionManifesto Create";
        $data['party'] = PoliticalParty::where('status', "Active")->pluck('name', 'id');

        return view('backend.ishtehaar.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'political_parties_id' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file_path' => 'required|mimes:pdf|max:2048',

        ]);
        $checkIshtehaar = ElectionManifesto::where('political_parties_id', $request->political_parties_id)->first();
        if (isset($checkIshtehaar)) {
            Session::flash('warning', 'Ads Already Exist.');
            return Redirect::back()->withInput();
        } else {

            $input['image'] = time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('electionmanifesto/images'), $input['image']);

            $input['file_path'] = time() . '.' . $request->file_path->getClientOriginalExtension();
            $request->file_path->move(public_path('electionmanifesto/pdf'), $input['file_path']);

            $input['political_parties_id'] = $request->political_parties_id;
            $input['title'] = $request->title;
            $input['short_details'] = $request->short_details;
            $input['is_active'] = $request->is_active;
            ElectionManifesto::create($input);
            Session::flash('message', 'Successfully Created.');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title']="Edit Ishtehaars";
        $data['editAIshtehaar']=ElectionManifesto::where('id',$id)->first();
        $data['party'] = PoliticalParty::where('status', 'Active')->pluck('name', 'id');

        return view('backend.ishtehaar.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'political_parties_id' => 'required',

        ]);

        $checkIshtehaar = ElectionManifesto::where('political_parties_id', $request->political_parties_id)->first();

        if (isset($checkAds)) {

            $IshtehaarId=$checkIshtehaar->political_parties_id;

        }else{
            $IshtehaarId=$request->political_parties_id;

        }

        $image = $request->file('image');
        $file = $request->file('file_path');

        $ishtehaarEdit = ElectionManifesto::findOrFail($id);

        if($file)
        {
            $request->validate([
                'file_path' => 'required|mimes:pdf|max:2048',
                ]);
            $file_path = public_path('electionmanifesto/pdf') . '/' . $ishtehaarEdit->file_path;
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            $finalName= time() . '.' . $file->clientExtension();
            $file->move(public_path('electionmanifesto/pdf'),$finalName);
        }
        else{
            $finalName=$ishtehaarEdit->file_path;
        }

        if($image)
        {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $file_path_img = public_path('electionmanifesto/images') . '/' . $ishtehaarEdit->image;
            if (file_exists($file_path_img)) {
                unlink($file_path_img);
            }
            $imageName= time() . '.' . $image->clientExtension();
            $image->move(public_path('electionmanifesto/images'),$imageName);
			dd($image);
        }
        else{
            $imageName=$ishtehaarEdit->image;
        }
        $ishtehaarEdit->file_path=$finalName;
        $ishtehaarEdit->image=$imageName;
        $ishtehaarEdit->political_parties_id = $IshtehaarId;
        $ishtehaarEdit->title = $request->title;
        $ishtehaarEdit->short_details = $request->short_details;
        $ishtehaarEdit->is_active = $request->is_active;
		
		
        $ishtehaarEdit->save();

        Session::flash('message', 'Successfully Update.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delIshtehaar=ElectionManifesto::find($id);

        $file_path = public_path('electionmanifesto/pdf') . '/' . $delIshtehaar->file_path;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        $file_path = public_path('electionmanifesto/images') . '/' . $delIshtehaar->image;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        $delIshtehaar->delete();

       // Session::flash('message', 'Successfully Deleted.');
        //return redirect()->back();
        return response()->json($delIshtehaar);
    }
}
