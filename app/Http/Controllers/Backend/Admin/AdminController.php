<?php

namespace App\Http\Controllers\Backend\Admin;


use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Auth;
use Helper;

class AdminController extends Controller
{
    protected $alltypeuserlist;

    public function __construct(User $alltypeuserlist)
    {
        $this->alltypeuserlist = $alltypeuserlist;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $data['title'] = 'User lists';
        $all = $this->alltypeuserlist;


        if ($request->has('name') && $request->name != null) {
            $all = $all->where('name', 'like', '%' . $request->name . '%');
        }
        if ($request->has('email') && $request->email != null) {
            $all = $all->where('email', '=', $request->email);
        }
        $data['alltypeofuser'] = $all->where('email', '!=', auth()->user()->email)->paginate(config('system.pagination.items_per_page'));
        $data['serial'] = Helper::managePagination($data['alltypeofuser']);
        if ($request->ajax()) {
            return view('backend.users.table_load', $data)->render();
        }

        return view('backend.users.index', $data);
    }


    public function create()
    {
        $data['title'] = 'Create user';
        return view('backend.users.create', $data);
    }

    public function store(Request $request)
    {


        $request->validate([
            'name' => 'required|string',
            'email' => 'required|nullable|unique:users|email',
            'password' => 'required|confirmed'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        Session::flash('message', 'User Successfully Created.');
        return redirect()->back();

    }

    public function show($id)
    {
        $data['showinfo'] = $this->alltypeuserlist->where('id', $id)->first();
        return view('backend.profile.profileInfo', $data);
    }

    public function edit($id)
    {
        $data['title'] = 'Edit user';
        $data['admin'] = User::findOrFail($id);
        return view('backend.users.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|nullable|email|unique:users,email,'.$id,
        ]);

        $user = User::findOrFail($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        Session::flash('message', 'User successfully updated.');
        return redirect()->back();
    }

    public function changeStatus(Request $req)
    {
        $status = User::find($req->id);
        $status->status = $req->status;
        $status->save();
        return response()->json(['status' => $status]);
    }


    public function destory($id)
    {
        $status = User::find($id);
        $status->delete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect()->back();
    }


}
