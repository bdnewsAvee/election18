<?php

namespace App\Http\Controllers\Backend\Gallery;

use App\Gallery;
use App\GalleryAlbums;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class VideoGallery extends Controller
{
    protected $allvideolist;

    public function __construct(Gallery $allvideolist)
    {
        $this->allvideolist = $allvideolist;
        $this->middleware('auth');
    }

    public function videoIndex(Request $request)
    {
        $data['title'] = "Video Gallery";
        $all = $this->allvideolist;

        if ($request->has('name') && $request->name != null) {
            $all = $all->where('name', 'like', '%' . $request->name . '%');
        }
        if ($request->has('gallery_albums_id') && $request->gallery_albums_id != null) {
            $all = $all->where('gallery_albums_id',  $request->gallery_albums_id );
        }
        $data['video'] = $all->with('albums')->where('type', 'Video')->paginate(config('system.pagination.items_per_page'));
        $data['serial'] = Helper::managePagination($data['video']);
        $data['albumsLists'] = GalleryAlbums::where('is_active', 1)->pluck('name','id');

        if ($request->ajax()) {
            return view('backend.gallery.video.table_load', $data)->render();
        }

        return view('backend.gallery.video.index', $data);
    }

    public function videoCreate()
    {
        for ($i = 0; $i < 50; $i++) {
            $data['sequence'][] = $i;
        }
        $data['title'] = "Save Video Gallery";
        $data['albumsLists'] = GalleryAlbums::where('is_active', 1)->pluck('name', 'id');

        return view('backend.gallery.video.create', $data);
    }

    public function videoSave(Request $request)
    {
        $this->validate($request, [
            'gallery_albums_id' => 'required',
            'name' => 'required',
            'path' => 'required',
            'sequence' => 'required|integer',
            'is_featured' => 'required'
        ]);
        $input['gallery_albums_id'] = $request->gallery_albums_id;
        $input['path'] = $request->path;
        $input['name'] = $request->name;
        $input['description'] = $request->description;
        $input['sequence'] = $request->sequence;
        $input['is_featured'] = $request->is_featured;
        $input['type'] = 'Video';
        $input['is_active'] = $request->is_active;

        Gallery::create($input);

        Session::flash('message', 'Add Successfully.');
        return redirect()->back();

    }

    public function videoEdit($id)
    {
        $data['title'] = "Edit Video Gallery";
        for ($i = 0; $i < 50; $i++) {
            $data['sequence'][] = $i;
        }
        $data['albumsLists'] = GalleryAlbums::where('is_active', 1)->pluck('name', 'id');
        $data['editVideo'] = Gallery::find($id);
        return view('backend.gallery.video.edit', $data);
    }

    public function updateVideo(Request $request, $id)
    {
        $this->validate($request, [
            'gallery_albums_id' => 'required',
            'name' => 'required',
            'path' => 'required',
            'sequence' => 'required|integer',
            'is_featured' => 'required'
        ]);
        $videoUpdate = Gallery::findOrFail($id);
        $videoUpdate->gallery_albums_id = $request->gallery_albums_id;
        $videoUpdate->path = $request->path;
        $videoUpdate->name = $request->name;
        $videoUpdate->description = $request->description;
        $videoUpdate->is_featured = $request->is_featured;
        $videoUpdate->sequence = $request->sequence;
        $videoUpdate->is_active=$request->is_active;
        $videoUpdate->save();

        Session::flash('message', 'Update Successfully.');
        return redirect()->back();
    }

    public function videoDelete($id){
        $delVideo=Gallery::find($id);
        $delVideo->delete();
      /*  Session::flash('message', 'Video removed successfully.');
        return redirect()->back();*/
        return response()->json($delVideo);

    }

}
