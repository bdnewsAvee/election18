<?php

namespace App\Http\Controllers\Backend\Gallery;

use App\Gallery;
use App\GalleryAlbums;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ImageGallery extends Controller
{

    protected $allimagelist;

    public function __construct(Gallery $allimagelist)
    {
        $this->allimagelist = $allimagelist;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data['title'] = "List of Gallery Images";
        $all = $this->allimagelist;

        if ($request->has('name') && $request->name != null) {
            $all = $all->where('name', 'like', '%' . $request->name . '%');
        }
        if ($request->has('gallery_albums_id') && $request->gallery_albums_id != null) {
            $all = $all->where('gallery_albums_id',  $request->gallery_albums_id );
        }
        $data['images'] = $all->with('albums')->where('type', 'Image')->paginate(config('system.pagination.items_per_page'));
        $data['serial'] = Helper::managePagination($data['images']);
        $data['albumsLists'] = GalleryAlbums::where('is_active', 1)->pluck('name','id');

        if ($request->ajax()) {
            return view('backend.gallery.image.table_load', $data)->render();
        }

        return view('backend.gallery.image.index', $data);
    }

    public function create(){
        $data['title']='Create Image Gallery';
        for ($i = 0; $i < 50; $i++) {
            $data['sequence'][] = $i;
        }
        $data['albumsLists'] = GalleryAlbums::where('is_active', 1)->pluck('name', 'id');
        return view('backend.gallery.image.create', $data);

    }

    public function uploadImage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'sequence' => 'required|integer',
            'gallery_albums_id' => 'required',

        ]);


        $input['path'] = time() . '.' . $request->image->getClientOriginalExtension();
        $request->image->move(public_path('gallery/images'), $input['path']);

        $input['gallery_albums_id'] = $request->gallery_albums_id;
        $input['name'] = $request->name;
        $input['description'] = $request->description;
        $input['sequence'] = $request->sequence;
        $input['is_featured'] = $request->is_featured;
        $input['type'] = 'Image';
        Gallery::create($input);
        Session::flash('message', 'Image Uploaded successfully.');
        return redirect()->back();

    }

    public function edit($id){
        $data['title']="Edit Image Gallery";
        for ($i = 0; $i < 50; $i++) {
            $data['sequence'][] = $i;
        }
        $data['editImage']=Gallery::find($id);
        $data['albumsLists'] = GalleryAlbums::where('is_active', 1)->pluck('name', 'id');
        return view('backend.gallery.image.edit', $data);

    }

    /**
     * Remove Image function
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyImage($id)
    {
        $delImage=Gallery::find($id);
        $file_path = public_path('gallery/images') . '/' . $delImage->path;
        if (file_exists($file_path) && $delImage->path!=null) {
            unlink($file_path);
        }
        $delImage->delete();
        return response()->json($delImage);

       /* return back()
            ->with('success', 'Image removed successfully.');*/
    }
}
