<?php

namespace App\Http\Controllers\Backend\Gallery;

use App\Gallery;
use App\GalleryAlbums;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class GalleryController extends Controller
{
    protected $allvideolist;

    public function __construct(Gallery $allvideolist)
    {
        $this->allvideolist = $allvideolist;
        $this->middleware('auth');
    }

    //------------------------------------Albums Manage Start---------------------------------
    protected $rules =
        [
            'name' => 'required|min:2|max:150',
            'description' => 'required|min:2|max:250'
        ];

    public function listAlbums()
    {
        $data['allAlbums'] = GalleryAlbums::all();
        return view('backend.gallery.albums.lists', $data);
    }

    public function listAlbumsSave(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $post = new GalleryAlbums();
            $post->name = $request->name;
            $post->description = $request->description;
            $post->save();
            return response()->json($post);
        }
    }

    public function show($id)
    {
        // $post = GalleryAlbums::findOrFail($id);

        //  return view('post.show', ['post' => $post]);
    }

    public function listAlbumsUpdate(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $post = GalleryAlbums::findOrFail($id);
            $post->name = $request->name;
            $post->description = $request->description;
            $post->save();
            return response()->json($post);
        }
    }

    public function listAlbumsDelete($id)
    {
        $post = GalleryAlbums::findOrFail($id);
        $post->delete();

        return response()->json($post);
    }

    public function changeStatus()
    {
        $id = Input::get('id');
        $post = GalleryAlbums::findOrFail($id);
        $post->is_active = !$post->is_active;
        $post->save();
        return response()->json($post);
    }

    //------------------------------------Albums Manage End---------------------------------


}
