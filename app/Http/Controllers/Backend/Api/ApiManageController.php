<?php

namespace App\Http\Controllers\Backend\Api;

use App\ApiManage;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ApiManageController extends Controller
{
    //


    public function editApi(){

        $data['title']="Update Api Data";
        $data['apiInfo']=ApiManage::latest('id')->first();
        $someArray = '{"Dogs":[{"category": "Companion dogs","name": "Chihuahua"}]}';
        if (empty($data['apiInfo'])) {
            ApiManage::insert(
                array(
                    'api_url'     =>   'Api Link here',
                    'api_data'   =>  json_decode($someArray)
                )
            );
        }
        return view('backend.api.edit',$data);
    }

    public function UpdateApi(Request $request,$id){
        $this->validate($request, [
            'api_url' => 'required',
            'api_data'=>'required',
        ]);

        if (isset($request->api_url)){
            $json=json_decode(Helper::request($request->api_url,null,$method=null),true);

        }else{
            $json=json_decode(Helper::request(config('system.api.home_news'),null,$method=null),true);

        }

        $apiData = ApiManage::findOrFail($id);
        $apiData->api_url = $request->api_url;
        $apiData->api_data = json_encode($json['items']);
        $apiData->save();

        Session::flash('message', 'Successfully Update.');
        return redirect()->back();
    }

    public function apiData(){

        $json = json_decode(Helper::request(config('system.api.home_news'),null, $method = null),true);
        $apiData = ApiManage::findOrFail(1);
        $apiData->api_url = config('system.api.home_news');
        $apiData->api_data = json_encode($json['items']);
        $apiData->save();
    }

}
