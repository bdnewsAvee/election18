<?php

namespace App\Http\Controllers\Backend\Opinion;

use App\Helpers\Helper;
use App\Opinion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class OpinionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['title'] = "Opinion Lists";
        $all = new Opinion();

        if ($request->has('title') && $request->title != null) {
            $all = $all->where('title', 'like', '%' . $request->title . '%');
        }
        $data['opinion'] = $all->paginate(config('system.pagination.items_per_page'));
        $data['serial'] = Helper::managePagination($data['opinion']);
        if ($request->ajax()) {
            return view('backend.opinion.table_load', $data)->render();
        }
        return view('backend.opinion.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Create Opinion";
        return view('backend.opinion.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image_url' => 'required',
            'opinion_details_link' => 'required'
        ]);

        $input['title'] = $request->title;
        $input['image_url'] = $request->image_url;
        $input['sequence'] = $request->sequence;
        $input['short_details'] = $request->short_details;
        $input['opinion_details_link'] = $request->opinion_details_link;
        $input['status'] = $request->status;
        Opinion::create($input);
        Session::flash('message', 'Successfully stored.');
        return redirect()->route('opinion_manage.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = "Update Opinion";
        for ($i = 0; $i < 50; $i++) {
            $data['sequence'][] = $i;
        }
       
        $data['opinionEdit'] = Opinion::find($id);
        //dd($data);
        return view('backend.opinion.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'image_url' => 'required',
            'opinion_details_link' => 'required'
        ]);

        $opinionEdit = Opinion::findOrFail($id);
        $opinionEdit->title = $request->title;
        $opinionEdit->image_url = $request->image_url;
        $opinionEdit->sequence = $request->sequence;
        $opinionEdit->short_details = $request->short_details;
        $opinionEdit->opinion_details_link = $request->opinion_details_link;
        $opinionEdit->status = $request->status;
        $opinionEdit->save();
        Session::flash('message', 'Successfully updated.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delOpinion=Opinion::find($id);
        $delOpinion->delete();
      //  Session::flash('message', 'Successfully Delete.');
        return response()->json($delOpinion);
        //return redirect()->back();
    }
}
