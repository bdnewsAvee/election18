<?php

namespace App\Http\Controllers\Backend;

use App\Districts;
use App\Divisions;
use App\Helpers\Helper;
use App\Seats;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SeatsController extends Controller
{
    //
    protected $allseatslist;

    public function __construct(Seats $allseatslist)
    {
        $this->allseatslist = $allseatslist;
        $this->middleware('auth');
    }

    public function getListofSeat(Request $request){

        $data['title']="List Seats";
        $all = $this->allseatslist;
		
		

        if ($request->has('name') && $request->name != null) {
            $all = $all->where('name', 'like', '%' . $request->name . '%')->orWhere('name_bn', 'like', '%' . $request->name . '%');
        }
        if ($request->has('division_id') && $request->division_id != null) {
            $all = $all->where('division_id',  $request->division_id  );
        }
        if ($request->has('district_id') && $request->district_id != null) {
            $all = $all->where('district_id',  $request->district_id  );

            $district=Districts::where('division_id',$request->division_id)->get();

            

            $data['district']=$district->pluck('bn_name','id');
        }else{
            $data['district']=Districts::pluck('bn_name','id');
        }
        $data['seatList'] = $all->with('getDivision','getDistrict')->paginate(25);
        $data['serial'] = Helper::managePagination($data['seatList']);
        $data['division']=Divisions::pluck('bn_name','id');
        
        if ($request->ajax()) {
            return view('backend.seats._table', $data)->render();
        }
        return view('backend.seats.lists',$data);
    }

    public function editSeat(Request $request,$id){
        $data['title']="Edit Seat";
        $data['edit']=Seats::with('getDivision','getDistrict')->where('id',$id)->first();
        $data['division']=Divisions::pluck('bn_name','id');
        $district=Districts::where('division_id',$data['edit']->division_id)->get();
        $data['district']=$district->pluck('bn_name','id');

        return view('backend.seats.edit',$data);

    }

    public function updateSeat(Request $request,$id){

        $this->validate($request, [
            'division_id' => 'required',
            'district_id' => 'required',
            'name_bn' => 'required',
            'constituency' => 'required',
            'status' => 'required',

        ]);
         $request->file('image');
        $image = $request->file('image');
         $seatUpdate = Seats::findOrFail($id);
        if($image)
        {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

              $file_path_img = public_path('seat/images') . '/' . $seatUpdate->image;

            if (file_exists($file_path_img) && $seatUpdate->image!=null) {
               unlink($file_path_img);
            }
            $imageName= time() . '.' . $image->clientExtension();
            $image->move(public_path('seat/images'),$imageName);
        }
        else{
            $imageName=$seatUpdate->image;
        }

        $seatUpdate->image=$imageName;
        $seatUpdate->division_id=$request->division_id;
        $seatUpdate->district_id=$request->district_id;
        $seatUpdate->name=$request->name;
        $seatUpdate->name_bn=$request->name_bn;
        $seatUpdate->description=$request->description;
        $seatUpdate->constituency=$request->constituency;
        $seatUpdate->status=$request->status;
        $seatUpdate->save();
        Session::flash('message', 'Successfully Update.');
        return redirect()->route('seats_lists');
    }

    public function districtAnyWhere(Request $r){

        switch($r->get('action')){
            case 'district':
                $dist=Districts::where('division_id',$r->get('id'))->get();
                echo '<option value="">জেলা নির্বাচন করুন</option>';
                foreach($dist as $d){
                    echo '<option value="'.$d->id.'">'.$d->bn_name.'</option>';
                }
            break;

        }

    }

}
