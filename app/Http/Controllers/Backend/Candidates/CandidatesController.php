<?php

namespace App\Http\Controllers\Backend\Candidates;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Districts;
use App\Divisions;
use App\Helpers\Helper;
use App\Seats;
use App\ElectionHistories;
use App\Candidates;
use App\PoliticalParty;
use App\Election;


class CandidatesController extends Controller
{
    protected $allcandidiates;
    public function __construct(Candidates $allcandidiates)
    {
        $this->allcandidiates = $allcandidiates;
        $this->middleware('auth');
    }

    
    public function index(Request $request)
    {
        $data['title'] = "Candidates Lists";
        $allCandidates = $this->allcandidiates;
        $data['candidates'] = $allCandidates->paginate(config('system.pagination.items_per_page'));
        $data['serial'] = Helper::managePagination($data['candidates']);
        $candidatesAll = $data['candidates'];
        foreach ($candidatesAll as $item) {
            $seat1 = Seats::where('constituency',$item['candidates_seat_id'])->first(); 
			$Election7 = Election::where('id',$item['candidates_election_id'])->first(); 
            $item['constituency_name'] = $seat1->name_bn;
			$item['elect_nam'] = $Election7->name_bn;
        }
		
		if ($request->ajax()) {
            return view('backend.candidates.table_load', $data)->render();
        }
		//dd($data);
        return view('backend.candidates.index', $data);		
    }

   
    public function create()
    {
        $data['title'] = "Create Candidates";
        $data['allseats']=Seats::pluck('name_bn','constituency');
		$data['all_political_party']=PoliticalParty::pluck('name','id');
		$data['elections_table']=Election::where('id','=', 1)->pluck('name_bn','id');
		return view('backend.candidates.create', $data);
    }

   
    public function store(Request $request)
    {
         $this->validate($request, [
            'candidates_name' => 'required',
			'candidates_name_en' => 'required',
            'candidates_election_id' => 'nullable',
            'candidates_seat_id' => 'nullable',
            'candidates_party' => 'nullable',
            'candidates_symbol' => 'nullable',
            'candidates_photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
              ]);
              
        $input['candidates_name'] = $request->candidates_name;
		$input['candidates_name_en'] = $request->candidates_name_en;
        $input['candidates_election_id'] = $request->candidates_election_id;
        $input['candidates_seat_id'] = $request->candidates_seat_id;
        $input['candidates_party'] = $request->candidates_party;
        $input['candidates_symbol'] = $request->candidates_symbol;
		if($request->candidates_photo !=''){
        $input['candidates_photo'] = time() . '.' . $request->candidates_photo->getClientOriginalExtension();
		$request->candidates_photo->move(public_path('candidates/images'), $input['candidates_photo']);
		}
        

		Candidates::create($input);
        Session::flash('message', 'Successfully stored.');
        return redirect()->route('candidates_manage.index');
    }

   
    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
		$data['title'] = "Update Candidates"; 
        
		$data['editImage']=Candidates::find($id);
        $data['allseats']=Seats::pluck('name_bn','constituency'); 
        $data['all_political_party']=PoliticalParty::pluck('name','id');
        $data['elections_table']=Election::where('id','=', 1)->pluck('name_bn','id');		
        
        $data['edit'] = Candidates::find($id);
        return view('backend.candidates.edit', $data);
    }

   
    public function update(Request $request, $id)
    {	
		 $this->validate($request, [
            'candidates_name' => 'required',
			'candidates_name_en' => 'required',
            'candidates_election_id' => 'nullable',
            'candidates_seat_id' => 'nullable',
            'candidates_party' => 'nullable',
            'candidates_symbol' => 'nullable',
            'candidates_photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
              ]);

        $edit = Candidates::findOrFail($id);
        $edit->candidates_name = $request->candidates_name;
		$edit->candidates_name_en = $request->candidates_name_en;
        $edit->candidates_election_id = $request->candidates_election_id;
        $edit->candidates_seat_id = $request->candidates_seat_id;
		$edit->candidates_party = $request->candidates_party;
        $edit->candidates_symbol = $request->candidates_symbol;
		 
		//dd($request->file('candidates_photo'));
		if($request->file('candidates_photo') !=''){
			
		  $candidates_photo = $request->file('candidates_photo');
		$edit->candidates_photo = time() . '.' . $request->candidates_photo->getClientOriginalExtension();
        $request->candidates_photo->move(public_path('candidates/images'), $edit->candidates_photo);
		 }
		else{
			
			if(($request->file('candidates_photo') =='') && $edit->candidates_photo !=''){
				$edit->candidates_photo = $edit->candidates_photo;
			}
			else {
		$edit->candidates_photo = $request->candidates_photo;
			}		
		   }  
        $edit->save();
        Session::flash('message', 'Successfully updated.');
        return redirect()->back();
    }

   
    public function destroy($id)
    {
        //
    }
}
