<?php

namespace App\Http\Controllers\Backend\ElectionHistories;

use App\Districts;
use App\Divisions;
use App\Helpers\Helper;
use App\Seats;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\ElectionHistories;
use App\Election;

class ElectionHistoriesController extends Controller
{
    protected $allelection_histories;
    public function __construct(ElectionHistories $allelection_histories)
    {
        $this->allelection_histories = $allelection_histories;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data['title'] = "Election Histories Lists";
		$allseats = new Seats();
		
		$data['seatselection_history'] = $allseats;
		$allElection_histories = $this->allelection_histories;
		
        $data['election_history'] = $allElection_histories->paginate(config('system.pagination.items_per_page'));
        $data['serial'] = Helper::managePagination($data['election_history']);
        if ($request->ajax()) {
            return view('backend.election_history.table_load', $data)->render();
        }	
	    foreach ($data['election_history'] as $item) {
            $Election1 = Election::where('id',$item['election_name'])->first(); 	
            $item['elect_name'] = $Election1->name_bn;
        }	
        return view('backend.election_history.index', $data);
    }

  
    public function create()
    {
        $data['title'] = "Create election_history";

        $data['allseats']=Seats::pluck('name_bn','constituency');
		$data['elections_table']=Election::where('id','>', 1)->pluck('name_bn','id');
		
        
       
		//dd($data['elections_table']);
        
        return view('backend.election_history.create', $data);
       
    }

   
    public function store(Request $request)
    {
        $this->validate($request, [
            'constituency_id' => 'required',
            'seat_brief' => 'nullable',
            'election_name' => 'required',
            'election_date' => 'nullable',
            'total_center' => 'nullable',
            'total_vote' => 'nullable',
            'male_vote' => 'nullable',
            'female_vote' => 'nullable',
            'vote_cast' => 'nullable',
            'valid_vote' => 'nullable',
            'invalid_vote' => 'nullable',
            'no_vote' => 'nullable',
            'winner_name' => 'nullable',
            'winning_party' => 'nullable',
            'winning_vote_count' => 'nullable',
            'winner_symbol' => 'nullable',
            'runner_up_name' => 'nullable',
            'runner_up_party' => 'nullable',
            'runner_up_symbol' => 'nullable',
            'runner_up_vote_count' => 'nullable'            
        ]);

        $input['constituency_id'] = $request->constituency_id;
        $input['seat_brief'] = $request->seat_brief;
        $input['election_name'] = $request->election_name;
        $input['election_date'] = $request->election_date;
        $input['total_center'] = $request->total_center;
        $input['total_vote'] = $request->total_vote;
        $input['male_vote'] = $request->male_vote;
        $input['female_vote'] = $request->female_vote;
        $input['vote_cast'] = $request->vote_cast;
        $input['valid_vote'] = $request->valid_vote;
        $input['invalid_vote'] = $request->invalid_vote;
        $input['no_vote'] = $request->no_vote;
        $input['winner_name'] = $request->winner_name;
        $input['winning_party'] = $request->winning_party;
        $input['winning_vote_count'] = $request->winning_vote_count;
        $input['winner_symbol'] = $request->winner_symbol;
        $input['runner_up_name'] = $request->runner_up_name;
        $input['runner_up_party'] = $request->runner_up_party;
        $input['runner_up_symbol'] = $request->runner_up_symbol;
        $input['runner_up_vote_count'] = $request->runner_up_vote_count;
        ElectionHistories::create($input);

        Session::flash('message', 'Successfully stored.');
        return redirect()->route('election_history.index');
    }

  
    public function show($id)
    {
        //
    }

   
	 
	
    public function edit($id)
    {
        $data['title'] = "Update election_history";
      
        $data['allseats']=Seats::pluck('name_bn','constituency');   
        $data['elections_table']=Election::pluck('name_bn','id');		
        $data['edit'] = ElectionHistories::find($id);
      
        return view('backend.election_history.edit', $data);
    }

   

  
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'constituency_id' => 'required',
            'seat_brief' => 'nullable',
            'election_name' => 'required',
            'election_date' => 'nullable',
            'total_center' => 'nullable',
            'total_vote' => 'nullable',
            'male_vote' => 'nullable',
            'female_vote' => 'nullable',
            'vote_cast' => 'nullable',
            'valid_vote' => 'nullable',
            'invalid_vote' => 'nullable',
            'no_vote' => 'nullable',
            'winner_name' => 'nullable',
            'winning_party' => 'nullable',
            'winning_vote_count' => 'nullable',
            'winner_symbol' => 'nullable',
            'runner_up_name' => 'nullable',
            'runner_up_party' => 'nullable',
            'runner_up_symbol' => 'nullable',
            'runner_up_vote_count' => 'nullable'
        ]);
 

        $edit = ElectionHistories::findOrFail($id);
        $edit->constituency_id = $request->constituency_id;
        $edit->seat_brief = $request->seat_brief;
        $edit->election_name = $request->election_name;
		$edit->election_date = $request->election_date;
        $edit->total_center = $request->total_center;
        $edit->total_vote =  $request->total_vote;
		
		$edit->male_vote = $request->male_vote;
        $edit->female_vote = $request->female_vote;
        $edit->vote_cast = $request->vote_cast;
		$edit->valid_vote = $request->valid_vote;
        $edit->invalid_vote = $request->invalid_vote;
        $edit->no_vote = $request->no_vote;
        $edit->winner_name = $request->winner_name;
        $edit->winning_party = $request->winning_party;
		
		$edit->winning_vote_count = $request->winning_vote_count;
        $edit->winner_symbol = $request->winner_symbol;
        $edit->runner_up_name = $request->runner_up_name;
		$edit->runner_up_party = $request->runner_up_party;
        $edit->runner_up_symbol = $request->runner_up_symbol;
        $edit->runner_up_vote_count = $request->runner_up_vote_count;
       
        $edit->save();
        Session::flash('message', 'Successfully updated.');
        
        return redirect()->back();
    }

    
    public function destroy($id)
    {
        $delElectionHistories=ElectionHistories::find($id);
        $delElectionHistories->delete();
      //  Session::flash('message', 'Successfully Delete.');
        return response()->json($delElectionHistories);
        //return redirect()->back();
    }
}
