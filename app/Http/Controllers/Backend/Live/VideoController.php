<?php

namespace App\Http\Controllers\Backend\Live;

use App\LiveVideo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class VideoController extends Controller
{
    //
    public function getEdit(){
        $data['title']="Update Live Link";
        $data['liveLink']=LiveVideo::latest('id')->first();

        if (empty($data['liveLink'])) {

            LiveVideo::insert(array('live_link'     =>   'Live Video Link Enter here'));
        }
        return view('backend.live.video.edit',$data);
    }

    public function UpdateLive(Request $request,$id){

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'live_link' => 'required',
            'status'=>'required'
        ]);
        $upLive = LiveVideo::findOrFail($id);
        $upLive->title=$request->title;
        $upLive->description=$request->description;
        $upLive->live_link=$request->live_link;
        $upLive->status=$request->status;
        $upLive->save();
        Session::flash('message', 'Successfully Update.');
        return redirect()->back();
    }
}
