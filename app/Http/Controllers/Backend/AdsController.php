<?php

namespace App\Http\Controllers\Backend;

use App\Ads;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AdsController extends Controller
{
    protected $alladslist;

    public function __construct(Ads $alladslist)
    {
        $this->alladslist = $alladslist;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['title']="Ads Lists";
        $all = $this->alladslist;

        if ($request->has('search') && $request->search != null) {
            $all = $all->where('page',$request->search);
        }

        $data['ads'] = $all->paginate(config('system.pagination.items_per_page'));
        $data['serial'] = Helper::managePagination($data['ads']);
        if ($request->ajax()) {
            return view('backend.ads.table_load', $data)->render();
        }
        return view('backend.ads.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title']="Ads Create";
//        return view('backend.ads.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'position_id' => 'required'
        ]);
        $checkAds=Ads::where('position_id',$request->position_id)->first();
        if (isset($checkAds)){
            Session::flash('warning', 'Ads Already Exist.');
            return Redirect::back()->withInput();
        }else{
            $input['position_id'] = $request->position_id;
            $input['position_name'] = AdsType::where('id',$request->position_id)->first()->type_name;
            $input['ads_code'] = $request->ads_code;
           // $input['image_path'] = $request->image_path;
           // $input['destination_path'] = $request->destination_path;
            $input['is_active'] = $request->is_active;
            Ads::create($input);
            Session::flash('message', 'Successfully Created.');
        }

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title']="Edit Ads";
        $data['editAds']=Ads::where('id',$id)->first();
        return view('backend.ads.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'js_code_lg' => 'required',
            'js_code_md' => 'required',
            'js_code_xs' => 'required',
            'ads_code' => 'required',
            'status' => 'required'
        ]);

        $adsEdit = Ads::findOrFail($id);
        $adsEdit->js_code_lg = $request->js_code_lg;
        $adsEdit->js_code_md = $request->js_code_md;
        $adsEdit->js_code_xs = $request->js_code_xs;
        $adsEdit->ads_code = $request->ads_code;
        $adsEdit->status = $request->status;
        $adsEdit->save();

        Session::flash('message', 'Successfully Update.');
        return redirect()->route('ads_manage.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $delAds=Ads::find($id);
        $delAds->delete();

        Session::flash('message', 'Successfully Deleted.');
        return redirect()->back();
    }
}
