<?php

namespace App\Http\Controllers\Frontend;
use App\Helpers\BanglaConverter;

use App\Ads;
use App\ApiManage;
use App\Gallery;
use App\GalleryAlbums;
use App\Helpers\Helper;
use App\ElectionManifesto;
use App\LiveVideo;
use App\Opinion;
use App\PoliticalParty;
use App\Seats;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ElectionHistories;
use App\Candidates;


class FrontendController extends Controller
{
    //

    public function home()
    {

        $apiData = ApiManage::latest('id')->first();
        $newsData = json_decode($apiData->api_data);

        if($newsData!=null){
        $data['slideData'] = array_slice($newsData, 0, 6);
        $data['ALlNews0ne'] = array_slice($newsData, 6, 6);
        $data['ALlNewsTwo'] = array_slice($newsData, 12, count($newsData)-12);
        }
        $data['video'] = Gallery::with('albums')->where('is_active', 1)
            ->where('type', 'Video')->orderBy('sequence', 'asc')
            ->where('is_featured', 1)->get();

        $data['image'] = Gallery::with('albums')->where('is_active', 1)
            ->where('type', 'Image')->orderBy('sequence', 'asc')
            ->where('is_featured', 1)->get();

        $data['opinion'] = Opinion::where('status', 'Published')->orderBy('sequence')->take(5)->get();

        $data['liveLink'] = LiveVideo::latest('id')->where('status', 'Active')->first();
        return view('welcome', $data);
    }

    //Api News Details
    public function getDetails($articalid)
    {
        $detailApiUrl = config('system.api.home_new_details') . $articalid;

        $data['apiDelData'] = json_decode(Helper::request($detailApiUrl, null, $method = null), true);
        $data['titla'] = $data['apiDelData']['title'] . 'bdnews24.com';
        return view('frontend.news-details', $data);
    }

    //Video Gallery

    public function getVideo()
    {
        $data['titile'] = "Video Gallery";
        $data['albumsLists'] = GalleryAlbums::with(['albumsGalleryHasone' => function ($q) {
            $q->where('is_active', 1)->where('type', 'Video');
        }])->where('is_active', 1)->get();
        return view('frontend.video-gallery', $data);
    }

    public function detailsVideoGallery($id)
    {

        $data['title'] = "Image Gallery Details";
        $data['album'] = GalleryAlbums::where('id', $id)->select('id','name')->first();
        $data['videoDetails'] = Gallery::where('gallery_albums_id', $id)->where('is_active', 1)
            ->where('type', 'Video')->get();
       $data['albumsLists'] = GalleryAlbums::with(['albumsGalleryHasone' => function ($q) {
            $q->where('is_active', 1)->where('type', 'Video');
        }])->where('is_active', 1)->where('id','!=',$id)->orderBy('id', 'DESC')->limit(12)->get();
        return view('frontend.image-video-details', $data);

    }

    //Image Gallery
    public function getImage()
    {

        $data['titile'] = "Image Gallery";
        $data['albumsLists'] = GalleryAlbums::with(['albumsGalleryHasone' => function ($q) {
            $q->where('is_active', 1)->where('type', 'Image');
        }])->where('is_active', 1)->get();
        return view('frontend.image-gallery', $data);

    }

    public function detailsImageGallery($id)
    {
        $data['title'] = "Image Gallery Details";
        $data['album'] = GalleryAlbums::where('id', $id)->select('id','name')->first();
        $data['imageDetails'] = Gallery::where('gallery_albums_id', $id)->where('is_active', 1)
            ->where('type', 'Image')->get();
        $data['albumsLists'] = GalleryAlbums::with(['albumsGalleryHasone' => function ($q) {
            $q->where('is_active', 1)->where('type', 'Image');
        }])->where('is_active', 1)->where('id','!=',$id)->orderBy('id', 'DESC')->limit(12)->get();
        return view('frontend.image-gallery-details', $data);

    }

    public function getIshtehaar()
    {
        $data['title'] = "ElectionManifesto";
        $data['parties']= PoliticalParty::with('relElectionManifesto')->where('status','Active')->get();

        return view('frontend.election_manifesto', $data);

    }

    public function getSeats($id)
    {
        $data['title'] = "Seats";
		
        $seat=Seats::where('constituency',$id)->first();
        $data['seat'] =$seat;
		
        $electionHistories=ElectionHistories::where('constituency_id',$id)->get();
          
        foreach ($electionHistories as $item) {
            $seat1 = Seats::where('constituency',$item['constituency_id'])->first();  
            $item['constituency_name'] = $seat->name_bn;
        }	
		$data['electionHistories'] =$electionHistories;
		
		$candidates=Candidates::where('candidates_seat_id',$id)->get();
	    foreach ($candidates as $item7) {
            $seat7 = Seats::where('constituency',$item7['candidates_seat_id'])->first();  	
            $item7['constituency_name7'] = $seat7->name_bn;
        }	
		$data['candidates'] =$candidates;
	
        return view('frontend.seats', $data);
    }
	 public function getElectionHistories($id)
    {
        $data['title'] = "ElectionHistories";
        foreach ($electionHistories as $item) {
            $seat1 = Seats::where('constituency',$item['constituency'])->first();
            $item['constituency_name'] = $seat1->name_bn;
        }
        $data['electionHistories'] =$electionHistories;
        return view('frontend.election_history', $data);
    }
}
