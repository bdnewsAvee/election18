<?php

namespace App\Providers;

use App\Ads;
use App\Divisions;
use App\ElectionManifesto;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        if (Schema::hasTable('ads') && Schema::hasTable('divisions') && Schema::hasTable('election_manifesto')) {
            // Code to create table

            $home_ads=Ads::where('status','Active')->get();
            $add_array=[];
            foreach ($home_ads as $value) {
                $add_array[$value->page][$value->position_name]=$value;
            }
            $data['ads']=$add_array;
            $data['divisions']=Divisions::with('seatByDivision')->orderBy('id','asc')->get();

            $data['ishtehaar']=ElectionManifesto::with('partyName')->where('is_active',1)->take(4)->get();


            \View::share('ads', $data['ads']);
            \View::share('divisions', $data['divisions']);
            \View::share('ishtehaar', $data['ishtehaar']);
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
