<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $table = 'ads';
    protected $fillable=['position_id','position_name','ads_code','image_path','destination_path','is_active'];
}
