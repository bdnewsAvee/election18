<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElectionHistories extends Model
{  
    protected $table = 'allelection_histories';
    protected $fillable = ['constituency_id','seat_brief','election_name','election_date','total_center','total_vote','male_vote','female_vote','vote_cast','valid_vote','invalid_vote','no_vote','winner_name','winning_party','winning_vote_count','winner_symbol','runner_up_name','runner_up_party','runner_up_symbol','runner_up_vote_count'];  
  
}
