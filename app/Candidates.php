<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Candidates extends Model
{
    //
	protected $table = 'candidates';
    protected $fillable = ['candidates_name','candidates_election_id','candidates_seat_id','candidates_party','candidates_symbol','candidates_photo']; 
	
	 public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
