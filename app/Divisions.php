<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisions extends Model
{
    protected $table = 'divisions';
    public $timestamps = true;
    public function seatByDivision()
    {
        return $this->hasMany(Seats::class, 'division_id', 'id');
    }
}
