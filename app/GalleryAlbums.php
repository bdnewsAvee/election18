<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryAlbums extends Model
{
    protected $table = 'gallery_albums';
    protected $fillable = ['name', 'description', 'is_active'];
    public static function getExcerpt($str, $startPos = 0, $maxLength = 50) {
        if(strlen($str) > $maxLength) {
            $excerpt   = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= ' [...]';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    public function albumsGallery()
    {
        return $this->hasMany(Gallery::class, 'gallery_albums_id', 'id');
    }
    public function albumsGalleryHasone()
    {
        return $this->hasOne(Gallery::class, 'gallery_albums_id', 'id');
    }
}
