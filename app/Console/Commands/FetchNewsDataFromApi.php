<?php

namespace App\Console\Commands;

use App\Http\Controllers\Backend\Api\ApiManageController;
use Illuminate\Console\Command;

class FetchNewsDataFromApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bdnews24:fetch-news-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch News List From BDNews24 API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $new=new ApiManageController();
      $new->apiData();
    }
}
