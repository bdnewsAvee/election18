<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'galleries';
    protected $fillable = ['gallery_albums_id', 'name', 'description','path','sequence','is_featured','type','is_active'];

    public function albums()
    {
        return $this->belongsTo(GalleryAlbums::class, 'gallery_albums_id', 'id');
    }
}
