<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsType extends Model
{
    protected $table = 'abs_types';
    protected $fillable=['type_name','is_active'];
}
