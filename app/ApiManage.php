<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiManage extends Model
{
    protected $table = 'api_manages';
    protected $fillable=['api_url','api_data'];
}
