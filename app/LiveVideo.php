<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveVideo extends Model
{
    protected $table = 'live_videos';
    protected $fillable = ['title','description','live_link','status'];
}
