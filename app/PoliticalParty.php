<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class PoliticalParty extends Model
{
    use SoftDeletes;
    protected $table = 'political_parties';
    protected $fillable = ['name','logo','description','status'];
    protected $dates = ['deleted_at'];
    public function relElectionManifesto()
    {
        return $this->hasOne('App\ElectionManifesto', 'political_parties_id', 'id');
    }

    /**
     * boot function for created and updated by user
     *
     * */

    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
