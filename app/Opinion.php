<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    protected $table = 'opinions';
    protected $fillable=['title','image_url','sequence','short_details','opinion_details_link','status'];
}
