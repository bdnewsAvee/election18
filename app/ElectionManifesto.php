<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElectionManifesto extends Model
{
    protected $table = 'election_manifesto';
    protected $fillable = ['political_parties_id','title','short_details','image','file_path','is_active'];
    public function partyName()
    {
        return $this->belongsTo(PoliticalParty::class, 'political_parties_id', 'id');
    }

}
