<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Login routes here
 * */
Route::get('login-election-portal','Auth\LoginController@showLoginForm')->name('login');
Route::post('login-election-portal','Auth\LoginController@login');
Route::post('logout','Auth\LoginController@logout')->name('logout');

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Backend Routes
Route::middleware('auth')->namespace('Backend')->group(function () {
    // Admin Routes
    Route::middleware('auth')->namespace('Admin')->group(function () {

        Route::resource('admin', 'AdminController', ['only' => [
            'index', 'create', 'store', 'edit', 'update', 'show'
        ]]);
        Route::get('admin/delete/{id}', 'AdminController@destory')->name('admin.delete');
        Route::post('admin/status', 'AdminController@changeStatus')->name('admin.status');
    });
    //Api Manage Controller
    Route::middleware('auth')->namespace('Api')->group(function () {

        Route::get('api_manage', 'ApiManageController@editApi')->name('api_manage');
        Route::put('api_manage/update/{id}', 'ApiManageController@UpdateApi')->name('api_manage.update');
    });
    // Gallery Routes
    Route::middleware('auth')->namespace('Gallery')->group(function () {
        //Image Galley manage
        Route::resource('gallery_image', 'ImageGallery', ['only' => ['index','create', 'edit', 'update',]]);
        Route::post('image-gallery', 'ImageGallery@uploadImage')->name('image-gallery');
        Route::delete('image-gallery/{id}', 'ImageGallery@destroyImage');

        //Albums manage
        Route::get('list_albums', 'GalleryController@listAlbums')->name('list_albums');
        Route::post('list_albums/save', 'GalleryController@listAlbumsSave')->name('list_albums.save');
        Route::post('list_albums/update/{id}', 'GalleryController@listAlbumsUpdate')->name('list_albums.update');
        Route::delete('list_albums/delete/{id}', 'GalleryController@listAlbumsDelete');
        Route::post('posts/changeStatus', array('as' => 'changeStatus', 'uses' => 'GalleryController@changeStatus'));

        //Video Gallery manage
        Route::get('gallery_video', 'VideoGallery@videoIndex')->name('gallery_video');
        Route::get('gallery_video/create', 'VideoGallery@videoCreate')->name('gallery_video.create');
        Route::post('gallery_video/save', 'VideoGallery@videoSave')->name('gallery_video.save');
        Route::get('gallery_video/edit/{id}', 'VideoGallery@videoEdit')->name('gallery_video.edit');
        Route::put('gallery_video/update/{id}', 'VideoGallery@updateVideo')->name('gallery_video.update');
        Route::delete('gallery_video/delete/{id}', 'VideoGallery@videoDelete')->name('gallery_video.delete');;

    });
    //Opinion Manage
    Route::namespace('Opinion')->group(function () {
        Route::resource('opinion_manage','OpinionController');
        /*Route::resource('opinion_manage','OpinionController', ['only' => [
            'index', 'create', 'store', 'edit', 'update', 'show'
        ]]);
        Route::delete('opinion_manage/delete/{id}','OpinionController@deleteOpinion')->name('opinion_manage.delete');*/
    });

    // Live Video Manage
    Route::middleware('auth')->namespace('Live')->group(function () {

        Route::get('live_video','VideoController@getEdit')->name('live_video');
        Route::put('live_video/update/{id}','VideoController@UpdateLive')->name('live_video.update');

    });

    //Ads Manage
    Route::resource('ads_manage', 'AdsController', ['only' => [
        'index', 'edit', 'update'
    ]]);
//    Route::get('ads_manage/delete/{id}','AdsController@destroy')->name('ads_manage.delete');

    //ElectionManifesto Manage
    Route::resource('ishtehaar_manage', 'IshtehaarController', ['only' => [
        'index', 'create', 'store', 'edit', 'update', 'show'
    ]]);
    Route::delete('ishtehaar_manage/delete/{id}','IshtehaarController@destroy')->name('ishtehaar_manage.delete');

    //Seats Manage
    Route::get('seats_lists','SeatsController@getListofSeat')->name('seats_lists');
    Route::get('seats_lists/edit/{id}','SeatsController@editSeat')->name('seats_lists.edit');
    Route::put('seats_lists/update/{id}','SeatsController@updateSeat')->name('seats_lists.update');
	
	  //candidates Manage
     Route::namespace('Candidates')->group(function () {
        Route::resource('candidates_manage','CandidatesController');
		});
	
    //election_history Manage
    Route::namespace('ElectionHistories')->group(function () {
        Route::resource('election_history','ElectionHistoriesController');
		});
});
Route::post('district-ajax', 'Backend\SeatsController@districtAnyWhere')->name('district.check_ajax');
// Frontend Routes
Route::namespace('Frontend')->group(function () {
    Route::get('/','FrontendController@home')->name('election.home');
    Route::get('news-details/{id}','FrontendController@getDetails')->name('news.details');
    Route::get('video-gallery','FrontendController@getVideo')->name('video.gallery');
    Route::get('video-gallery/details/{id}','FrontendController@detailsVideoGallery')->name('video.details');
    Route::get('image-gallery','FrontendController@getImage')->name('image.gallery');
    Route::get('image-gallery/details/{id}','FrontendController@detailsImageGallery')->name('image.details');
    Route::get('election-manifesto','FrontendController@getIshtehaar')->name('election.manifesto');
    Route::get('seat/{id}/{title?}','FrontendController@getSeats')->name('seat');
	Route::get('election_history/{id}/{title?}','FrontendController@getElectionHistories')->name('election_history');

});

Route::get('/changePassword', 'HomeController@showChangePasswordForm')->name('changePassword');
Route::post('/changePassword', 'HomeController@changePassword')->name('changePassword');