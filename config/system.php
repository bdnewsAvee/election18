<?php

return [
    'pagination' => [
        'items_per_page' => 10
    ],
    'api'=>[
        'home_news'=>'https://bangla.bdnews24.com/json/v0_01/adn/news-list.jsp?token=adn91$D1G1yH',
        'home_new_details'=>'https://bangla.bdnews24.com/json/v0_01/adn/story.jsp?token=adn91$D1G1yH&id='
    ]
 ];