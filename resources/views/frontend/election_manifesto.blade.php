@extends('layouts.frontend.master')

@section('content')
    <section id="gallery-section ">
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    <div class="col-md-12">
                        <div class="section-title">
                            <h3>ইশতেহার</h3>
                        </div>
                    </div>
                    <div class='list-group gallery'>
                        <div class="row">
                            @if(count($parties)>0)

                            @php $row=0; @endphp
                            @foreach($parties as $party)
                                @php $row++ @endphp
                                <div class='col-md-6' style="padding: 15px 10px;">
                                    <a href="@if(isset($party->relElectionManifesto) && $party->relElectionManifesto!=null) {{url('electionmanifesto/pdf/')}}/{{ $party->relElectionManifesto->file_path }} @else # @endif" target="_blank">
                                        <div class="row box">
                                            <div class="col-md-3 co-sm-2 col-xs-2"style="padding: 0px">
                                                @if($party->logo!=null)
                                                <img src="{{ $party->logo }}" style="width: 100%;height: 80px;"></div>
                                                @else
                                                <img src="{{ asset('frontend/images/nologo.png') }}" style="width: 100%;height: 80px;"></div>
                                                @endif

                                            <div class="col-md-9 co-sm-10 col-xs-10">

                                                <h4>{!! $party->name !!}</h4>
                                            </div>
                                    </a>
                                    </div>

                                </div> <!-- col-6 / end -->
                                @if($row == 2)
                                    @php $row=0; @endphp
                        </div>
                        <div class="row">
                            @endif
                            @endforeach
                        @else
                            <h3 class="text-center">কোন তথ্য পাওয়া যায়নি</h3>
                        @endif

                        </div>
                    </div> <!-- list-group / end -->

                </div>
                <div class="col-md-4">
                    @include('layouts.frontend._right-aside')
                </div>
            </div>
        </div>
    </section>

@endsection