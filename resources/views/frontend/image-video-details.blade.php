@extends('layouts.frontend.master')

@section('content')

    <section id="photo-album-section">
        <div class="container">
            <div class="row">

                <div class="col-md-8">



    <div class="col-md-12">
        <div class="section-title">
            <h3>{{ $album->name }}</h3>
        </div>
    </div>

            <div class="row">
                @php $row=0; @endphp
                    @foreach($videoDetails as $k=>$videoDetail)
                        @php $row++ @endphp

                    <div class="col-md-6">

                        <div class="item {{ ($k==0)? 'active':''}} thumbnail fancybox videoIframe">
                            {!! html_entity_decode($videoDetail->path) !!}
                            <div class="text-justify">
                                <p>{{$videoDetail->name}}</p>
                            </div>
                        </div>
                    </div>

                        @if($row >= 2)
                            @php $row=0; @endphp
                        </div>
                            <div class="row">
                        @endif

                    @endforeach


            </div>


                    <div class="col-md-12">
                        <div class="section-title">
                            <h3>সাম্প্রতিক ভিডিও</h3>
                        </div>
                    </div>
                    <div class='list-group gallery'>
                        @foreach($albumsLists as $k=>$video)
                            @if(isset($video->albumsGalleryHasone))

                                <div class='col-md-4 videoIframe'>

                                    {!! html_entity_decode( $video->albumsGalleryHasone->path) !!}


                                    <div class='text-justify'>
                                        <p><a href="{{ route('video.details',$video->id) }}"> {{$video->name}}</a> </p>
                                    </div> <!-- text-right / end -->

                                </div> <!-- col-6 / end -->
                            @endif
                        @endforeach
                    </div> <!-- list-group / end -->
                </div>
                <div class="col-md-4">

                    @include('layouts.frontend._right-aside')
                </div>
            </div>
        </div>
    </section>

@endsection

