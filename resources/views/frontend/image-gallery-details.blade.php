@extends('layouts.frontend.master')

@section('content')
    <section id="photo-album">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h3>{{ $album->name }}</h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="carousel-photo-generic" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            @foreach($imageDetails as $k=>$imageDetail)

                            <div class="item {{ ($k==0)? 'active':''}}">
                                <img src="{{asset('gallery/images')}}/{{$imageDetail->path}}" alt="...">
                                <div class="carousel-caption">
                                    <h2>{{$imageDetail->name}}</h2>
                                </div>
                            </div>
                            @endforeach


                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-photo-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-photo-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="photo-album-section">
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    <div class="col-md-12">
                        <div class="section-title">
                            <h3>সাম্প্রতিক ছবিঘর</h3>
                        </div>
                    </div>
                    <div class='list-group gallery'>
                        @foreach($albumsLists as $k=>$image)

                            @if(isset($image->albumsGalleryHasone))
                                <div class='col-md-4'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="{{url('image_gallery/details/')}}/{{$image->id}}">

                                        <img class="img-responsive" alt=""
                                             src="{{asset('gallery/images')}}/{{$image->albumsGalleryHasone->path}}"/>
                                        <div class='text-justify'>
                                            <p> {{$image->name}}</p>
                                        </div> <!-- text-right / end -->
                                    </a>
                                </div> <!-- col-6 / end -->
                            @endif
                        @endforeach
                    </div> <!-- list-group / end -->
                </div>
                <div class="col-md-4">

               @include('layouts.frontend._right-aside')
                </div>
            </div>
        </div>
    </section>

@endsection

