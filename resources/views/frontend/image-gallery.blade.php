@extends('layouts.frontend.master')

@section('content')
    <section id="gallery-section ">
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    <div class="col-md-12">
                        <div class="section-title">
                            <h3>ছবিঘর</h3>
                        </div>
                    </div>
                    <div class='list-group gallery'>
                        @if(count($albumsLists)>0)
                        @php $row=0; @endphp
                        <div class="row">
                        @foreach($albumsLists as $k=>$image)
                                @php $row++ @endphp
                            @if(isset($image->albumsGalleryHasone))
                                <div class='col-md-6'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="{{ route('image.details',$image->id) }}">

                                        <img class="img-responsive" alt=""
                                             src="{{asset('gallery/images')}}/{{$image->albumsGalleryHasone->path}}"/>
                                        <div class='text-justify'>
                                            <p> {{$image->name}}</p>
                                        </div> <!-- text-right / end -->
                                    </a>
                                </div> <!-- col-6 / end -->
                            @endif
                                @if($row >= 2)
                                    @php $row=0; @endphp
                             </div>
                            <div class="row">
                        @endif
                        @endforeach
                            </div>

                        @else
                            <h3 class="text-center">কোন তথ্য পাওয়া যায়নি</h3>
                        @endif
                    </div> <!-- list-group / end -->
                </div>
                <div class="col-md-4">
                    @include('layouts.frontend._right-aside')

                </div>
            </div>
        </div>
    </section>

@endsection