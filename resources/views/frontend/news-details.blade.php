@extends('layouts.frontend.master')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 pb-2 padding-0 padding-right-5">

                <h2>{!! $apiDelData['title'] !!}</h2>
                <div class="byline">
                    <p class="margin-0">{!! $apiDelData['byline'] !!} </p>
                    <p>
                        <strong>Published :</strong> {!!  date('Y-m-d  H:i:s', strtotime($apiDelData['publishedDate']))!!} BdST
                        <strong>Updated :</strong> {!! date('Y-m-d  H:i:s', strtotime($apiDelData['modifiedDate'])) !!} BdST
                    </p>
                </div>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @foreach($apiDelData['relatedPictures'] as $k=>$slide)
                            <li data-target="#carousel-example-generic" data-slide-to="{{$k}}"
                                class="{{ ($k==0)? 'active':''}}"></li>
                        @endforeach
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">

                        @foreach( $apiDelData['relatedPictures'] as $k=>$slide)
                            <div class="item {{ ($k==0)? 'active':''}}">
                                <img src="{{ $slide['image'] }}" alt="{{ $apiDelData['title'] }}" style="width: 100%;">
                                <div class="carousel-caption">
                                    <p>{{$slide['caption']}}</p>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button"
                       data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button"
                       data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div style="width: 100% ;padding:20px 0">
                    <h5 class="sub-title">{!! $apiDelData['leadtext'] !!}</h5>
                    {!!  $apiDelData['body'] !!}
                </div>

                <div class="text-center">


                    <!-- Facebook -->
                    <a href="http://www.facebook.com/sharer.php?u={{ url()->current() }}" target="_blank">
                        <img src="https://d30fl32nd2baj9.cloudfront.net/media/2017/04/16/1492360615_facebook_circle.png/BINARY/1492360615_facebook_circle.png" alt="Facebook" />
                    </a>

                    <!-- Twitter -->
                    <a href="https://twitter.com/share?url={{ url()->current() }}&amp;text={!! $apiDelData['title'] !!}" target="_blank">
                        <img src="https://d30fl32nd2baj9.cloudfront.net/media/2017/04/16/1492360746_twitter_circle.png/BINARY/1492360746_twitter_circle.png" alt="Twitter" />
                    </a>

                    <!-- Google+ -->
                    <a href="https://plus.google.com/share?url={{ url()->current() }}" target="_blank">
                        <img src="https://d30fl32nd2baj9.cloudfront.net/media/2017/04/15/1492274418_google-plus.png/BINARY/1492274418_google-plus.png" alt="Google" />
                    </a>

                    <!-- LinkedIn -->
                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ url()->current() }}&amp;source=bdnews24.com" target="_blank">
                        <img src="https://d30fl32nd2baj9.cloudfront.net/media/2017/04/15/1492274361_linkedin_circle.png/BINARY/1492274361_linkedin_circle.png" alt="LinkedIn" />
                    </a>

                    <!-- Pinterest -->
                   <a href="http://pinterest.com/pin/create/button/?url={{ url()->current() }}&amp;media=bdnews24.com&amp;description={!! $apiDelData['title'] !!}"  target="_blank">
                        <img src="https://d30fl32nd2baj9.cloudfront.net/media/2017/04/15/1492274620_pinterest_circle.png/BINARY/1492274620_pinterest_circle.png" alt="Pinterest" />
                    </a>

                </div>
            </div>
            <div class="col-md-4 padding-0 padding-left-5"><img src="">
                @include('layouts.frontend._right-aside')

            </div>
        </div>
    </div>


@endsection

   @push('metaData')
                <link rel="title" href="{!!  $apiDelData['title']!!}"/>
                <link rel="description" href="{!!  $apiDelData['leadtext'] !!}"/>
                <link rel="keywords" href="{!!  $apiDelData['metaKeywords'] !!}"/>
                <link rel="canonical" href="{!!  $apiDelData['canonical'] !!}"/>
                <meta property="og:url" content="{!!  $apiDelData['canonical'] !!}"/>
                <meta property="og:title" content="{!! $apiDelData['title'] !!}"/>
                <meta property="og:description" content="{!!  $apiDelData['leadtext'] !!}"/>
                <meta property="og:image" content="{!!  $apiDelData['relatedPictures'][0]['image'] !!}"/>
                <meta property="og:type" content="article"/>
    @endpush