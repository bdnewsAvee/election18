@extends('layouts.frontend.master')

@section('content')
    <section id="gallery-section ">
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    <div class="col-md-12">
                        <div class="section-title">
                            <h3>ভিডিও গ্যালারি</h3>
                        </div>
                    </div>
                    <div class='list-group gallery'>
                        @if(count($albumsLists)>0)
                        <div class="row">
                            @php $row=0; @endphp
                            @foreach($albumsLists as $k=>$video)
                                @if(isset($video->albumsGalleryHasone))
                                    @php $row++ @endphp
                                    <div class='col-md-6 videoIframe' style="margin-bottom:10px;">

                                        <div style="position:relative;">
                                            <a href="{{ route('video.details',$video->id) }}" ><div style="background-color:rgba(0,0,0,0.4);height: 100%;width:100%; position:absolute;top:0;left:0;"></div></a>
                                            {!! html_entity_decode( $video->albumsGalleryHasone->path) !!}
                                        </div>



                                        <div class='text-justify' style="border:1px solid #ccc;">
                                            <p style="padding: 5px"><a href="{{ route('video.details',$video->id) }}"> {{$video->name}}</a> </p>
                                        </div> <!-- text-right / end -->

                                    </div> <!-- col-6 / end -->
                                @endif
                                @if($row > 2)
                                    @php $row=0;
                                    @endphp
                        </div>
                        <div class="row">
                            @endif
                            @endforeach
                        </div>
                        @else
                            <h3 class="text-center">কোন তথ্য পাওয়া যায়নি</h3>
                        @endif

                    </div> <!-- list-group / end -->

                </div>
                <div class="col-md-4">
                        @include('layouts.frontend._right-aside')
                </div>
            </div>
        </div>
    </section>

@endsection