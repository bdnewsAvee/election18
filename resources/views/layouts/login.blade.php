<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="{!! asset('img/adn_favicon.png') !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ isset($title)?$title:'bdnews24 election portal' }}</title>


    <!-- Bootstrap core CSS -->
    <link href="{!! asset('css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/bootstrap-reset.css') !!}" rel="stylesheet">
    <!--external css-->
    <link href="{!! asset('assets/font-awesome/css/font-awesome.css') !!}" rel="stylesheet"/>
    <!-- Custom styles for this template -->
    <link href="{!! asset('css/style.css" rel="stylesheet') !!}">
    <link href="{!! asset('css/style-responsive.css') !!}" rel="stylesheet"/>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{!! asset('js/html5shiv.js') !!}"></script>
    <script src="{!! asset('js/respond.min.js') !!}"></script>
    <![endif]-->
    <style>
        .form-signin h2.form-signin-heading {
            background: none !important;
            color: black !important;
        }
    </style>
</head>
<body>
<div class="login-body" style="text-align: center;">
    <div class="container">

        @yield('content')
    </div>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{!! asset('js/jquery.js') !!}"></script>
    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
</div>
</body>
</html>
