<div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
        <li>
            <a class="{{ request()->segment(1) == 'home' ? 'active' : null }}" href="{!! route('home') !!}">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'admin' ? 'active' : null }}" >
                <i class="fa fa-user"></i>
                <span>Users</span>
            </a>
            <ul class="sub">
                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a href="{{ route('admin.create') }}"> Create</a></li>

                <li class="{{ request()->segment(2) == 'index' ? 'active' : null }}"><a href="{{ route('admin.index') }}"> List</a></li>
            </ul>
        </li>
        <li>
            <a class="{{ request()->segment(1) == 'list_albums' ? 'active' : null }}" href="{!! route('list_albums') !!}">
                <i class="fa   fa-book"></i>
                <span>Manage Albums</span>
            </a>
        </li>
        <li>
            <a class="{{ request()->segment(1) == 'api_manage' ? 'active' : null }}" href="{!! route('api_manage') !!}">
                <i class="fa fa-asterisk"></i>
                <span>Api Manage</span>
            </a>
        </li>
        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'gallery_image' ? 'active' : null }}" >
                <i class="fa fa-camera-retro"></i>
                <span>Gallery</span>
            </a>
            <ul class="sub">

                <li class="{{ request()->segment(2) == 'index' ? 'active' : null }}"><a href="{{ route('gallery_image.index') }}"> Image</a></li>
                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a href="{{ route('gallery_video.create') }}">Create Video</a></li>
                <li class="{{ request()->segment(2) == 'create' ? 'gallery_video' : null }}"><a href="{{ route('gallery_video') }}">List Video</a></li>

            </ul>
        </li>

    </ul>
    <!-- sidebar menu end-->
</div>
