<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.frontend._head')
</head>

<body>

<!--header start-->
<header>
    @include('layouts.frontend._header')
</header>
<!--header end-->

<!--main content start-->

@yield('content')

<!--main content end-->
<!--footer start-->
@include('layouts.frontend._footer')
<!--footer end-->

@include('layouts.frontend._scripts')
</body>
</html>
