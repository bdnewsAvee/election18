<footer class="footer-bs">
    <div class="container">
        <div class="row">
            <div class="col-md-3 hidden-xs footer-brand animated fadeInLeft">
                   <a href="https://newsletter.bdnews24.com/" target="_blank" class="btn btn-default btn-lg m-top-15"  ><span class="glyphicon glyphicon-envelope"></span> নিউজলেটারের জন্য নিবন্ধন করুন</a>
            </div>

            <div class="col-md-9 hidden-xs">
                <div class="col-md-5 footer-nav animated fadeInUp">
                    <ul class="list">
                        <li>
                            <h2> খবর </h2>
                            <ul class="list first-list" >
                                <li >
                                    <a href="https://bangla.bdnews24.com/" target="_blank">হোম</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/bangladesh/" target="_blank">বাংলাদেশ</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/business/" target="_blank">বাণিজ্য</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/politics/" target="_blank">রাজনীতি</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/economy/" target="_blank">অর্থনীতি</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/world/" target="_blank">বিশ্ব</a>
                                </li>

                                <li>
                                    <a href="https://bangla.bdnews24.com/environment/" target="_blank">পরিবেশ</a>
                                </li>


                                <li>
                                    <a href="https://bangla.bdnews24.com/samagrabangladesh/" target="_blank">সমগ্র বাংলাদেশ</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/ctg/" target="_blank">চট্টগ্রাম</a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2 footer-nav animated fadeInUp">
                    <ul class="list">
                        <li>
                            <h2>
                                মতামত
                            </h2>
                            <ul class="list">
                                <li>
                                    <a href="https://opinion.bdnews24.com/bangla/" target="_blank">
                                        মতামত </a>
                                </li>
                                <li></li>
                            </ul>
                        </li>
                    </ul>

                </div>
                <div class="col-md-2 footer-nav animated fadeInUp">
                    <ul class="list">
                        <li >
                            <h2>
                                খেলা
                            </h2>
                            <ul class="list">
                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/" target="_blank">
                                        খেলা
                                    </a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/cricket/" target="_blank">
                                        ক্রিকেট
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2 footer-nav animated fadeInUp">
                    <ul class="list">
                        <li>
                            <h2>
                                অন্যান্য </h2>
                            <ul class="list">
                                <li>
                                    <a href="https://hello.bdnews24.com/" target="_blank">
                                        হ্যালো
                                    </a>
                                </li>

                                <li>
                                    <a href="https://tube.bdnews24.com/" target="_blank">
                                        টিউব
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 visible-xs">
                <a href="https://newsletter.bdnews24.com/" target="_blank" class="btn btn-default btn-lg" style="width: 100%"><span class="glyphicon glyphicon-envelope"></span> নিউজলেটারের জন্য নিবন্ধন করুন</a>

            </div>
            <div class="col-xs-12 visible-xs">
                <div class="col-xs-6 footer-nav animated fadeInUp">
                    <ul class="list">
                        <li>
                            <h2> খবর </h2>
                            <ul class="list first-list" >
                                <li >
                                    <a href="https://bangla.bdnews24.com/">হোম</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/bangladesh/" target="_blank">বাংলাদেশ</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/business/" target="_blank">বাণিজ্য</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/politics/" target="_blank">রাজনীতি</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/economy/" target="_blank">অর্থনীতি</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/world/" target="_blank">বিশ্ব</a>
                                </li>

                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/environment/" target="_blank">পরিবেশ</a>
                                </li>


                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/samagrabangladesh/" target="_blank">সমগ্র বাংলাদেশ</a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/ctg/" target="_blank">চট্টগ্রাম</a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-6 footer-nav animated fadeInUp">
                    <ul class="list">
                        <li>
                            <h2>
                                মতামত
                            </h2>
                            <ul class="list">
                                <li>
                                    <a href="https://opinion.bdnews24.com/bangla/" target="_blank">
                                        মতামত </a>
                                </li>
                                <li></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="list">
                        <li >
                            <h2>
                                খেলা
                            </h2>
                            <ul class="list">
                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/" target="_blank">
                                        খেলা
                                    </a>
                                </li>
                                <li>
                                    <a href="https://bangla.bdnews24.com/sport/" target="_blank">
                                        ক্রিকেট
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="list">
                        <li>
                            <h2>
                                অন্যান্য </h2>
                            <ul class="list">
                                <li>
                                    <a href="https://hello.bdnews24.com/" target="_blank">
                                        হ্যালো
                                    </a>
                                </li>

                                <li>
                                    <a href="https://tube.bdnews24.com/" target="_blank">
                                        টিউব
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <div>


        </div>
    </div>
    <!-- Copyright -->
    <div class="footer-copyright text-center">© @php echo date('Y') @endphp Copyright: bdnews24. All rights reserved.

                <a href="https://bangla.bdnews24.com/disclaimer_bangla/" target="_blank" title="Disclaimer &amp; Privacy Policy">
                    Disclaimer &amp; Privacy Policy
                </a> |

                <a href="https://aboutus.bdnews24.com/" target="_blank" title="About us">
                    About us
                </a> |

                <a href="https://aboutus.bdnews24.com/contact/" target="_blank" title="Contact us">
                    Contact us
                </a>

    </div>
    <!-- Copyright -->

</footer>