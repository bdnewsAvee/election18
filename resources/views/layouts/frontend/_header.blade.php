{{--Update design--}}
<section>

    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 xs-center padding-0">
                    <ul class="top-bar-left">
                        <li >@php $date_time = new App\DateTime; @endphp
                            {{ $date_time->BanglaDate() }},
                            <script type="text/javascript" src="http://bangladate.appspot.com/index5.php"></script>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->

                <div class=" col-md-6 col-sm-6 col-xs-12 social-link xs-center padding-0">
                    <ul class="pull-right">
                        <li class="active"><a href="https://www.facebook.com/bdnews24" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://twitter.com/bdnews24com" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/bangladesh-news-24-hours-ltd/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="https://plus.google.com/u/0/+bdnews24" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
</section>
<!--start header add section-->
@if(isset($ads['Universal']) && isset($ads['Universal']['TOP']))
    <section id="top-add" class="add-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12 padding-0 no-padding">
                    <div class="top-add-banner">
                        <div class="text-center">
                            {!! html_entity_decode($ads['Universal']['TOP']->ads_code) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
<!--end header add section-->

<!--start logo  section-->
<div class="container" >
    <div class="row">
        <div class="col-md-12 padding-0 padding-bottom-0-sm padding-right-0-sm padding-left-0-sm">
            <div class="top-logo-banner">
                <img src="{{ asset('frontend/images/election_2018_banner.png') }}" class="img-responsive" id="banner">
            </div>
        </div>
    </div>
</div>
<!--end logo  section-->

<!--====== Menu bar -->
<nav class="menu-bar navbar-default">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar1" class="collapse navbar-collapse">
                <div class="menu pull-left col-md-12 padding-0 no-padding">
                    <ul class="nav navbar-nav main-menu">
                        <li><a href="https://bdnews24.com/" target="_blank" title="bdnews24.com"><img src="{{asset('favicon.ico')}}" style="height: 20px"> bdnews24.com</a></li>
                        <li class="{{ request()->segment(1) == '' ? 'menu-active' : null }}" title="বাংলাদেশ"><a href="{{ route('election.home') }}">বাংলাদেশ</a></li>
                        <li class="{{ request()->segment(1) == 'image-gallery' ? 'menu-active' : null }}" title="ছবিঘর"><a href="{{ route('image.gallery') }}">ছবিঘর</a></li>
                        <li class="{{ request()->segment(1) == 'video-gallery' ? 'menu-active' : null }}" title="ভিডিও"><a href="{{ route('video.gallery') }}">ভিডিও</a></li>
{{--                        <li class="{{ request()->segment(1) == 'electionmanifesto' ? 'menu-active' : null }}" title="ইশতেহার"><a href="{{ route('election.manifesto') }}">ইশতেহার</a></li>--}}
                        <li class="padding-top-button-10 hover-off"><button class="btn btn1" id="divition-menu-btn" title="আসন">আসন <i class="fas fa-arrow-down"></i></button></li>
                    </ul>
                </div>
            </div><!--/.navbar-collapse -->

            <div id="divition" class="divition-menu" style="display:none;">
                <ul class="nav navbar-nav">
                    @foreach($divisions as $k=>$divison )
                        <li class="dropdown mega-dropdown">
                            <a href="#" class="dropdown-toggle level2" data-toggle="dropdown">
                                {{$divison->bn_name}} <i class="fas fa-plus-circle"></i>
                            </a>
                            <ul class="dropdown-menu mega-dropdown-menu column-count-6">
                                @foreach($divison->seatByDivision as $seat)
                                    <li style="float: none!important;">
                                        <a href="{{ route('seat',[$seat->constituency,$seat->name_bn]) }}">{{ $seat->name_bn }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
                <!-- /.nav-collapse -->
            </div>
        </div>
    </div>
</nav>
