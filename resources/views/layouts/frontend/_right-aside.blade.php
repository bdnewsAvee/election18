{{--<div class="row">
    <div class="col-md-12">
        <div class="section-title">
            <h3>যার যেমন প্রতিশ্রুতি</h3>

        </div>

    </div>
    <div class="col-md-12">
        <div id="carousel-commitment-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @foreach($ishtehaar as $k=>$v)
                <div class="item {{ ($k==0)? 'active':''}}">
                    <img src="{{ asset('electionmanifesto/images') }}/{{$v->image}}"  alt="{{$v->title}}"  style="height: 315px;width: 100%;">
                    <div class="carousel-caption">
                      <h4><a style="color: white" href="{{asset('electionmanifesto/pdf')}}/{{$v->file_path}}" target="_blank">{{$v->partyName->name}}</a></h4>
                    </div>
                </div>
                @endforeach

            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-commitment-generic" role="button"
               data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-commitment-generic" role="button"
               data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>--}}
@if(isset($ads['Universal']) && isset($ads['Universal']['RIGHT-SIDE-UP-WINDOW-SECTION']))
    <div class="row">
        <div class="col-md-12">
            <div class="right-add-banner">
                {!! html_entity_decode($ads['Universal']['RIGHT-SIDE-UP-WINDOW-SECTION']->ads_code) !!}
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="section-title">
            <h3>জানালা</h3>
        </div>
    </div>
    <div class="col-md-12">
        <div class="janala">
            <div class="janala-inner">
                জানালা
            </div>
        </div>
    </div>
</div>
@if(isset($ads['Universal']) && isset($ads['Universal']['RIGHT-SIDE-DOWN-WINDOW-SECTION']))
    <div class="row">
        <div class="col-md-12">
            <div class="right-add-banner">
                {!! html_entity_decode($ads['Universal']['RIGHT-SIDE-DOWN-WINDOW-SECTION']->ads_code) !!}
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="section-title">
            <h3>Hashcode</h3>

        </div>

    </div>
    <div class="col-md-12">
        <div class="hashcode">
            <div class="hashcode-inner">
                Hashcode
            </div>
        </div>
    </div>
</div>