
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{  asset('frontend/js/bootstrap.min.js')  }}"></script>
<script src="{{  asset('frontend/owl/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.touchSwipe.min.js') }}"></script>
<script type="text/javascript">

    $(".carousel").swipe({

        swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

            if (direction == 'left') $(this).carousel('next');
            if (direction == 'right') $(this).carousel('prev');

        },
        allowPageScroll:"vertical"

    });
    $(".videoCarousel").swipe({

        swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

            if (direction == 'left') owl.trigger('next.owl.carousel');
            if (direction == 'right') owl.trigger('prev.owl.carousel', [300]);

        },
        allowPageScroll:"vertical"

    });


</script>

@stack('js')
<script>
    // $('.divition-menu').hide();
    $('#divition-menu-btn').click(function (e) {
        e.preventDefault();
        $('.divition-menu').toggle();
    });


    $(document).mouseup(function (e){

        var container = $(".divition-menu");

        if (!container.is(e.target) && container.has(e.target).length === 0){

            container.fadeOut();

        }
    });


    /*jQuery(document).on('click', '.mega-dropdown', function(e) {
        e.stopPropagation()
    });*/


    /*Slider scripts*/
    $('.carousel').carousel();
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        touchDrag:true,
        mouseDrag:true,
        // pullDrag:true,
        // freeDrag:true,
        responsive:{
            0:{
                items:1,
                nav:false
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items:5,
                nav:false,
                loop:false
            }
        }
    });

    // Go to the next item
    $('.customNextBtn').click(function(e) {
        owl.trigger('next.owl.carousel');
        e.preventDefault();
    });
    // Go to the previous item
    $('.customPrevBtn').click(function(e) {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl.carousel', [300]);
        e.preventDefault();
    });

</script>
<!--<script>
    $(".explorebtn").click(function () {
        $(".map-overlay").hide();
        $(".map-close").show();
    });
    $(".map-close").click(function () {
        $(".map-overlay").show();
        $(".map-close").hide();
    });

</script>-->
