<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ isset($title)?$title:'bdnews24 election portal' }}</title>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="{{ asset('frontend/owl/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/owl/css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/style2.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
@stack('metaData')
@stack('css')
<!--header part starts-->
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
    var w = window.innerWidth;
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>
<script>
    if(w <= 480){
        googletag.cmd.push(function() {
            @foreach($ads as $layer)
                    @foreach($layer as $jsxs)
            {!! $jsxs->js_code_xs !!}
            @endforeach @endforeach googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs();
            googletag.enableServices();
        });
    }else  if (w <= 768 && w > 480 ) {
        googletag.cmd.push(function() {
            @foreach($ads as $layer)
            @foreach($layer as $jsxs)
            {!! $jsxs->js_code_md !!}
            @endforeach @endforeach googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs();
            googletag.enableServices();
        });
    }else  if (w <= 1905 && w > 768 ) {
        googletag.cmd.push(function() {
            @foreach($ads as $layer)
            @foreach($layer as $jsxs)
            {!! $jsxs->js_code_lg !!}
            @endforeach @endforeach googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs();
            googletag.enableServices();
        });
    }
</script>
