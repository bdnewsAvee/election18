<div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
        <li>
            <a class="{{ request()->segment(1) == 'home' ? 'active' : null }}" href="{!! route('home') !!}">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li>
            <a class="{{ request()->segment(1) == 'api_manage' ? 'active' : null }}" href="{!! route('api_manage') !!}">
                <i class="fa fa-asterisk"></i>
                <span>Api Manage</span>
            </a>
        </li>
        <li>
            <a class="{{ request()->segment(1) == 'seats_lists' ? 'active' : null }}"
               href="{!! route('seats_lists') !!}">
                <i class="fa fa-plus-square-o"></i>
                <span>Seats  Lists</span>
            </a>
        </li>
	
		
		 <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'election_history' ? 'active' : null }}">
                <i class="fa fa-gavel"></i>
                <span>Election History</span>
            </a>
            <ul class="sub">

                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a
                            href="{{ route('election_history.create') }}"> Create</a></li>
                <li class="{{ request()->segment(2) == 'list' ? 'active' : null }}"><a
                            href="{{ route('election_history.index') }}">Lists</a></li>

            </ul>
        </li>

		 <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'candidates_manage' ? 'active' : null }}">
                <i class="fa fa-gavel"></i>
                <span>Candidates</span>
            </a>
            <ul class="sub">

                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a
                            href="{{ route('candidates_manage.create') }}"> Create</a></li>
                <li class="{{ request()->segment(2) == 'list' ? 'active' : null }}"><a
                            href="{{ route('candidates_manage.index') }}">Lists</a></li>

            </ul>
        </li>

		
		
		
        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'opinion_manage' ? 'active' : null }}">
                <i class="fa fa-gavel"></i>
                <span>Opinion</span>
            </a>
            <ul class="sub">

                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a
                            href="{{ route('opinion_manage.create') }}"> Create</a></li>
                <li class="{{ request()->segment(2) == 'index' ? 'active' : null }}"><a
                            href="{{ route('opinion_manage.index') }}">Lists</a></li>

            </ul>
        </li>


        {{--    <li class="sub-menu">
                <a href="javascript:;" class="{{ request()->segment(1) == 'ads_manage' ? 'active' : null }}" >
                    <i class="fa fa-gavel"></i>
                    <span>Ads </span>
                </a>
                <ul class="sub">

                    <li class="{{ request()->segment(2) == 'index' ? 'active' : null }}"><a href="{{ route('ads_manage.create') }}"> Create</a></li>
                    <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a href="{{ route('ads_manage.index') }}">Lists</a></li>

                </ul>
            </li>--}}

        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'ishtehaar_manage' ? 'active' : null }}">
                <i class="fa fa-gavel"></i>
                <span>Ishtehaar </span>
            </a>
            <ul class="sub">

                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a
                            href="{{ route('ishtehaar_manage.create') }}"> Create</a></li>
                <li class="{{ request()->segment(2) == 'index' ? 'active' : null }}"><a
                            href="{{ route('ishtehaar_manage.index') }}">Lists</a></li>

            </ul>
        </li>
        <li>
            <a class="{{ request()->segment(1) == 'list_albums' ? 'active' : null }}"
               href="{!! route('list_albums') !!}">
                <i class="fa   fa-book"></i>
                <span>Manage Albums</span>
            </a>
        </li>

        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'gallery_video' ? 'active' : null }}">
                <i class="fa fa-video-camera"></i>
                <span>Video Gallery</span>
            </a>
            <ul class="sub">

                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a
                            href="{{ route('gallery_video.create') }}">Create Video</a></li>
                <li class="{{ request()->segment(2) == 'gallery_video' ? 'active' : null }}"><a
                            href="{{ route('gallery_video') }}">List Video</a></li>

            </ul>
        </li>
        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'gallery_image' ? 'active' : null }}">
                <i class="fa fa-picture-o"></i>
                <span>Image Gallery</span>
            </a>
            <ul class="sub">
                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a
                            href="{{ route('gallery_image.create') }}"> Create</a></li>

                <li class="{{ request()->segment(2) == 'index' ? 'active' : null }}"><a
                            href="{{ route('gallery_image.index') }}"> Lists</a></li>

            </ul>
        </li>

        <li>
            <a class="{{ request()->segment(1) == 'live_video' ? 'active' : null }}" href="{!! route('live_video') !!}">
                <i class="fa fa-circle" style="color:#FF5757"></i>
                <span>Live Video</span>
            </a>
        </li>
        <li>
            <a class="{{ request()->segment(1) == 'ads_manage' ? 'active' : null }}"
               href="{{ route('ads_manage.index') }}">
                <i class="fa fa-plus-square-o"></i>
                <span>Ads</span>
            </a>
        </li>
        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'admin' ? 'active' : null }}">
                <i class="fa fa-user"></i>
                <span>Users</span>
            </a>
            <ul class="sub">
                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a
                            href="{{ route('admin.create') }}"> Create</a></li>

                <li class="{{ request()->segment(2) == 'index' ? 'active' : null }}"><a
                            href="{{ route('admin.index') }}"> List</a></li>
            </ul>
        </li>

    </ul>
    <!-- sidebar menu end-->
</div>
