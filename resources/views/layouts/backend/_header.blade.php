<div class="sidebar-toggle-box">
    <i class="fa fa-bars"></i>
</div>
<!--logo start-->
<a href="{!! route('home') !!}" class="logo"> Election <span>Portal</span></a>
<!--logo end-->
<div class="nav notify-row" id="top_menu">
    <!--  notification start -->
    <ul class="nav top-menu">
        <!-- settings start -->

        <!-- notification dropdown end -->
    </ul>
    <!--  notification end -->
</div>
<div class="top-nav ">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <li>
            {{--<input type="text" class="form-control search" placeholder="Search">--}}
        </li>
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                {{--<img alt="" src="{!! asset('img/avatar1_small.jpg') !!}">--}}
                <span class="username"><i class="fa fa-user"></i> {{Auth::user()->name}}</span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <div class="log-arrow-up"></div>
                <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li><a href="{{route("changePassword")}}"><i class="fa fa-key"></i> Change <br>Password</a></li>
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </li>
        <li class="sb-toggle-right">
            {{--<i class="fa  fa-align-right"></i>--}}
        </li>
        <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
</div>