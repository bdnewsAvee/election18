@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                Update Opinion
            </header>
            <div class="panel-body">
                @include('layouts.backend._validationErrorMessages')
                {!! Form::model($opinionEdit,['route'=>['opinion_manage.update',$opinionEdit->id],'method'=>'put','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}
                <div class="col-md-8 col-md-offset-2">
                @include('backend.opinion._form')
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            {!! Form::submit('Update',['class'=>'btn btn-success btnCreate pull-left']) !!}
                            <a href="{{ route('opinion_manage.index') }}" class="btn btn-warning pull-right" onclick="return confirm('Are you confirm ?')"> <i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
</div>
@endsection
@push('css')
@endpush
@push('js')
    <!--this page  script only-->
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>
    @include('backend.opinion._script')
@endpush