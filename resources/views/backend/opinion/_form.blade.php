<div class="form-group">
    {!! Html::decode(Form::label('title','Title <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label text-right']))!!}
    <div class="col-lg-8">
        <div class="iconic-input right">
            {!! Form::text('title',null,['class'=>'form-control','required','placeholder'=>'Write title here','required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('path','Image Url <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::textarea('image_url',null,['cols'=>5,'rows'=>2,'class'=>'form-control','required','placeholder'=>'Write image url here','required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('opinion_details_link','Destination Path <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::textarea('opinion_details_link',null,['cols'=>5,'rows'=>2,'class'=>'form-control','required','placeholder'=>'Write destination url here']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('sequence','Sequence  ',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">
        <div class="iconic-input right">
            {!! Form::number('sequence',null,['class'=>'form-control','placeholder'=>'Select Sequence','min'=>0]) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('opinion_details_link','Short Description <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::textarea('short_details',null,['cols'=>5,'rows'=>5,'class'=>'form-control' ]) !!}

        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('is_active','Status <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">

        <div class="iconic-input right" style="display: inline-flex;">

            <div class="radio">
                <label>
                    {!! Form::radio('status','Unpublished',['class'=>'form-control','required','id'=>'optionsRadios2'])!!}
                    Unpublished
                </label>
            </div>
            <div class="radio">
                <label>
                    {!! Form::radio('status','Published',['class'=>'form-control','required','id'=>'optionsRadios1','checked'])!!}
                    Published
                </label>
            </div>
        </div>

    </div>
</div>
