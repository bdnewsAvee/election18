@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <header class="panel-heading">
                    Create Opinion
                </header>
                <div class="panel-body">
                    @include('layouts.backend._validationErrorMessages')
                    {!! Form::open(['route'=>'opinion_manage.store','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}
                    <div class="col-md-8 col-md-offset-2">
                        @include("backend.opinion._form")
                        <div class="form-group">

                            <div class="col-md-8 col-md-offset-2">
                                <a href="{{ route('opinion_manage.index') }}" class="btn btn-warning pull-right" onclick="return confirm('Are you confirm ?')"> <i class="fa fa-arrow-left"></i> Back</a>
                                {!! Form::submit('Save',['class'=>'btn btn-success btnCreate pull-left']) !!}
                            </div>

                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')

@endpush
@push('js')
    <!--this page  script only-->
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>

    @include('backend.opinion._script')
@endpush