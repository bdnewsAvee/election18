<table class="display table table-bordered table-striped" id="load">
    <thead>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Image</th>
        <th>Sequence</th>
        <th>Status</th>

        <th class="hidden-phone">Actions</th>
    </tr>


    </thead>
    <tbody>
    @foreach($opinion as $k=>$v)
        <tr class="item{{$v->id}}">
            <td>{{ $serial++ }}</td>
            <td>
                <a class=" btn btn-outline-primary"  href="{{ $v->opinion_details_link }}" target="_blank">
                    <h4>{{ $v->title }}</h4>
                </a>
                <p>{!! $v->short_details !!}</p>
            </td>
            <td>
                <a class=" btn btn-outline-primary"  href="{{ $v->opinion_details_link }}" target="_blank">
                    <img src="{{ $v->image_url }}" height="150px" width="150px">
                </a>
            </td>
            <th> {{ ($v->sequence == null)? 'No':$v->sequence }}</th>
            <th>{{ $v->status }}</th>

            <td>
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"
                            aria-expanded="false">Actions <span class="caret"></span></button>
                    <ul role="menu" class="dropdown-menu float-right">
                        <li>
                            <a href="{{route("opinion_manage.edit",$v->id)}}"><i class="fa fa-edit"></i> Edit</a>
                        </li>
                        <li>
                           {{-- <a href="{{route("opinion_manage.delete",$v->id)}}"><i class="fa fa-trash-o"></i> Delete</a>--}}
                            <a class="delete-modal"  data-id="{{$v->id}}" data-title="{{$v->title}}" href="#"><i class="fa fa-trash-o"></i>Delete</a>

                        </li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<ul class="pagination pagination-lg pull-right">{{$opinion->links()}}</ul>


<!-- Modal form to delete a form -->
<div id="deleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <h3 class="text-center">Are you sure you want to delete the following post?</h3>
                <br/>
                <form class="form-horizontal" role="form">
                    <div class="form-group" style="display: none">
                        <label class="control-label col-sm-2" for="id">ID:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id_delete" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        {{--  <label class="control-label col-sm-2" for="title">Title:</label>--}}
                        <div class="col-sm-10">
                            <h3 id="title" class="text-center text-success"></h3>
                            <input type="hidden" class="form-control" id="title_delete" disabled>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                        <span id="" class='fa fa-trash-o'></span> Delete
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='fa fa-times'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>