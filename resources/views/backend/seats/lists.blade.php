@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Seats Lists
                </header>
                <div class="panel-body">
                    <div class="content">
                        <div class="row">
                            {{ Form::open(['method'=>'get']) }}
                            <div class="col-md-3 ">
                                {!! Form::select('division_id',$division,\Illuminate\Support\Facades\Input::get('division_id'),['class'=>'form-control', 'placeholder'=>'বিভাগ  নির্বাচন করুন','onchange'=>"districtControl('district',this.value)" ]) !!}

                            </div>
                            <div class="col-md-3 ">
                                {!! Form::select('district_id',$district,\Illuminate\Support\Facades\Input::get('district_id'),['class'=>'form-control','placeholder'=>'জেলা নির্বাচন করুন','id'=>'district' ]) !!}
                            </div>
                            <div class="col-md-3  ">
                                {{ Form::text('name',\Illuminate\Support\Facades\Input::get('name'),['class'=>'form-control js-example-basic-single','placeholder'=>'Seats Name']) }}
                            </div>


                            <div class="col-md-3">
                                {{ Form::button('<i class="fa fa-search"></i> Search',['type'=>'submit','class'=>'btn btn-primary']) }}
                                <a href="{{route('seats_lists')}}" class="btn btn-warning"><i
                                            class="fa fa-refresh"></i> Clear</a>
                            </div>

                            {{ Form::close() }}
                        </div>

                    </div>
                    <br>
                    <div class="table-responsive userlistofadmin">
                        @if (count($seatList) > 0)
                            @include('backend.seats._table')
                        @else
                            <h3 style="text-align: center">No Data Found</h3>
                        @endif
                    </div>

                </div>
            </section>
        </div>
    </div>

@endsection
@push('css')
    <link href="{{ asset('css/table-responsive.css') }}" rel="stylesheet"/>
    <style>
        iframe{width:100% !important;}
    </style>
@endpush
@push('js')
    @include('backend.seats._script')
@endpush