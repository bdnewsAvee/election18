<div class="col-md-4">
    <div class="form-group">
        {!! Html::decode(  Form::label('division_id','Division <sup> <span class="red">*</span></sup>',['class'=>'col-lg-3 col-sm-4 control-label']) )!!}

        <div class="col-lg-9 col-sm-8">
            <div class="iconic-input right">

                {!! Form::select('division_id',$division,null,['class'=>'form-control','required','placeholder'=>'বিভাগ  নির্বাচন করুন','onchange'=>"districtControl('district',this.value)"]) !!}

            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Html::decode(  Form::label('district_id','District <sup> <span class="red">*</span></sup>',['class'=>'col-lg-3 col-sm-4 control-label']) )!!}

        <div class="col-lg-9 col-sm-8">
            <div class="iconic-input right">

                {!! Form::select('district_id',$district,null,['class'=>'form-control','required','placeholder'=>'জেলা নির্বাচন করুন','id'=>'district']) !!}

            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Html::decode(  Form::label('name','Name EN ',['class'=>'col-lg-3 col-sm-4 control-label']) )!!}
        <div class="col-lg-9 col-sm-8">
            <div class="iconic-input right">
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'English Name']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Html::decode(  Form::label('name','Name BN <sup> <span class="red">*</span></sup>',['class'=>'col-lg-3 col-sm-4 control-label']) )!!}
        <div class="col-lg-9 col-sm-8">
            <div class="iconic-input right">
                {!! Form::text('name_bn',null,['class'=>'form-control','placeholder'=>'Bangla Name','required']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Html::decode(  Form::label('title','Seat No <sup> <span class="red">*</span></sup>',['class'=>'col-lg-3 col-sm-4 control-label']) )!!}
        <div class="col-lg-9 col-sm-8">
            <div class="iconic-input right">
                {!! Form::text('constituency',null,['class'=>'form-control','required','placeholder'=>'Type Seat No']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4">Image Upload <sup> <span class="red">*</span></sup></label>
        <div class="col-md-8">
                <strong style="color: red">Image Width: 360px <br>Image Height: 280px</strong>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                    @if($edit->image)
                        <img src="{{asset('seat/images')}}/{{$edit->image}}" alt=""/>
                    @else
                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                    @endif
                </div>
                <div class="fileupload-preview fileupload-exists thumbnail"
                     style="max-width: 200px; max-height: 150px; line-height: 20px;">

                </div>
                <div>
                  <span class="btn btn-white btn-file">
                  <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                  <span class="fileupload-exists"><i
                              class="fa fa-undo"></i> Change</span>
                  <input type="file" class="default" name="image"/>
                  </span>
                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i  class="fa fa-trash"></i> Remove</a>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="col-md-8">
    <div class="form-group">
        {!! Html::decode(  Form::label('opinion_details_link','Description',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
        <div class="col-md-10">
            <div class="from-input right">
                {!! Form::textarea('description',null,['cols'=>10,'rows'=>10,'class'=>'form-control ','placeholder'=>'Short Description']) !!}

            </div>
        </div>
    </div>


    <div class="form-group">
        {!! Html::decode(  Form::label('status','Status <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
        <div class="col-lg-10">

            <div class="iconic-input right" style="display: inline-flex;">
                <div class="radio">
                    <label>
                        {!! Form::radio('status','Active',['class'=>'form-control','required','id'=>'optionsRadios1','checked'])!!}

                        Active
                    </label>
                </div>
                <div class="radio">
                    <label>
                        {!! Form::radio('status','Inactive',['class'=>'form-control','required','id'=>'optionsRadios2'])!!}

                        Inactive
                    </label>
                </div>

            </div>

        </div>
    </div>

</div>


@push('js')
    <script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endpush