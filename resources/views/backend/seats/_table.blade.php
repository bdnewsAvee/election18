<table class="display table table-bordered table-striped" id="load">
    <thead>
    <tr>
        <th>ID</th>
        <th>Division</th>
        <th>District</th>
        <th>Seat Name</th>
        <th>Seat No</th>
        <th>Published?</th>

        <th class="hidden-phone">Actions</th>
    </tr>


    </thead>
    <tbody>
    @foreach($seatList as $k=>$v)
        <tr>
            <td>{{ $serial++ }}</td>
            <td>{{$v->getDivision->bn_name}}</td>
            <td>{{ $v->getDistrict['bn_name'] }}</td>
            <td> {{$v->name_bn}}</td>
            <td> {{$v->constituency}} </td>
            <th>{{($v->status == "Active")? 'Active':'Inactive'}}</th>

            <td>
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"
                            aria-expanded="false">Actions <span class="caret"></span></button>
                    <ul role="menu" class="dropdown-menu float-right">
                        <li>
                            <a href="{{route("seats_lists.edit",$v->id)}}"><i class="fa fa-edit"></i> Edit</a>
                        </li>

                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<ul class="pagination pagination-lg pull-right">{{$seatList->links()}}</ul>

