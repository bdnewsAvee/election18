@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="  col-md-12">
        <section class="panel">
            <header class="panel-heading">
                Update Seats Information
            </header>
            <div class="panel-body">
                @include('layouts.backend._validationErrorMessages')
                {!! Form::model($edit,['route'=>['seats_lists.update',$edit->id],'method'=>'put','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}

                <div class="col-md-12">
                    <div class="row">
                        @include('backend.seats._form')

                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-xs-6 ">
                                {!! Form::submit('Update',['class'=>'btn btn-success btnCreate pull-right']) !!}
                            </div>
                            <div class="col-xs-6 ">

                                <a href="{{ route('seats_lists') }}" class="btn btn-warning pull-left"> <i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                        </div>

                    </div>



                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
</div>
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.css')}}" />
@endpush
@push('js')
    <script type="text/javascript" src="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
    <!--this page  script only-->
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>
    @include('backend.seats._script')
@endpush