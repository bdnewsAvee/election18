@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">

                    <ul>
                        <li><i class="fa fa-file-text-o"></i> All Albums Lists</li>
                        <a href="#" class="add-modal">
                            <li>Add a Albums</li>
                        </a>
                    </ul>
                </header>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="postTable"
                           style="visibility: hidden;">
                        <thead>
                        <tr>
                            <th valign="middle">ID</th>
                            <th>Title</th>
                            <th>Content</th>
                            <th>Published?</th>
                            <th>Last updated</th>
                            <th>Actions</th>
                        </tr>
                        {{ csrf_field() }}
                        </thead>
                        <tbody>
                        @foreach($allAlbums as $post)
                            <tr class="item{{$post->id}} @if($post->is_active) warning @endif">
                                <td>{{$post->id}}</td>
                                <td>{{$post->name}}</td>
                                <td>
                                    {{App\GalleryAlbums::getExcerpt($post->description)}}
                                </td>
                                <td class="text-center"><input type="checkbox" class="published" data-id="{{$post->id}}"
                                                               @if ($post->is_active) checked @endif></td>
                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->diffForHumans() }}</td>
                                <td>
                                    <button class="show-modal btn btn-success" data-id="{{$post->id}}"
                                            data-title="{{$post->name}}" data-content="{{$post->description}}">
                                        <span class="fa fa-eye"></span> Show
                                    </button>
                                    <button class="edit-modal btn btn-info" data-id="{{$post->id}}"
                                            data-title="{{$post->name}}" data-content="{{$post->description}}">
                                        <span class="fa fa-edit"></span> Edit
                                    </button>
                                    <button class="delete-modal btn btn-danger" data-id="{{$post->id}}"
                                            data-title="{{$post->name}}" data-content="{{$post->description}}">
                                        <span class="fa fa-trash"></span> Delete
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <!-- Modal form to add a post -->
                    <div id="addModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="title">Title:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="title_add" autofocus>
                                                <small>Min: 2, Max: 32, only text</small>
                                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="content">Content:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="content_add" cols="40"
                                                          rows="5"></textarea>
                                                <small>Min: 2, Max: 128, only text</small>
                                                <p class="errorContent text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success add" data-dismiss="modal">
                                            <span id="" class='fa  fa-check'></span> Add
                                        </button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                                            <span class='fa fa-times'></span> Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal form to show a post -->
                    <div id="showModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="id">ID:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="id_show" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="title">Title:</label>
                                            <div class="col-sm-10">
                                                <input type="name" class="form-control" id="title_show" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="content">Content:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="content_show" cols="40" rows="5"
                                                          disabled></textarea>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                                            <span class='fa fa-times'></span> Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal form to edit a form -->
                    <div id="editModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="id">ID:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="id_edit" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="title">Title:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="title_edit" autofocus>
                                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="content">Content:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="content_edit" cols="40"
                                                          rows="5"></textarea>
                                                <p class="errorContent text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                                            <span class='fa fa-check'></span> Edit
                                        </button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                                            <span class='fa fa-times'></span> Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal form to delete a form -->
                    <div id="deleteModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <h3 class="text-center">Are you sure you want to delete the following post?</h3>
                                    <br/>
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="id">ID:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="id_delete" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="title">Title:</label>
                                            <div class="col-sm-10">
                                                <input type="name" class="form-control" id="title_delete" disabled>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                                            <span id="" class='fa fa-trash-o'></span> Delete
                                        </button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                                            <span class='fa fa-times'></span> Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </section>
        </div>
    </div>
@endsection
@push('css')
    <!-- icheck checkboxes -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.css">

    <style>
        .panel-heading {
            padding: 0;
        }

        .panel-heading ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .panel-heading li {
            float: left;
            border-right: 1px solid #bbb;
            display: block;
            padding: 14px 16px;
            text-align: center;
        }

        .panel-heading li:last-child:hover {
            background-color: #ccc;
        }

        .panel-heading li:last-child {
            border-right: none;
        }

        .panel-heading li a:hover {
            text-decoration: none;
        }

        .table.table-bordered tbody td {
            vertical-align: baseline;
        }

        /* icheck checkboxes */
        .iradio_flat-yellow {
            background: url(https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.png) no-repeat;
        }
    </style>
    <link href="{{ asset('css/table-responsive.css') }}" rel="stylesheet"/>
@endpush
@push('js')
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    @include('backend.gallery.albums._script')
@endpush