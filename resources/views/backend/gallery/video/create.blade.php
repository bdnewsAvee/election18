@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <header class="panel-heading">
                    Add Video Gallery Information
                </header>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    {!! Form::open(['route'=>'gallery_video.save','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}

                    <div class="col-md-8 col-md-offset-2">
                         @include("backend.gallery.video._form")
                        <div class="form-group">

                            <div class="col-md-8 col-md-offset-2">
                                {!! Form::submit('Save',['class'=>'btn btn-success btnCreate pull-right']) !!}
                                <a href="{{ route('gallery_video') }}" class="btn btn-warning pull-left"> <i
                                            class="fa fa-arrow-left"></i> Back</a>
                            </div>

                        </div>
                    </div>
                        {!! Form::close() !!}
                </div>
            </div>
            </section>
        </div>
    </div>
@endsection
@push('css')

@endpush
@push('js')
    <!--this page  script only-->
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>

    @include('backend.gallery.video._script')
@endpush