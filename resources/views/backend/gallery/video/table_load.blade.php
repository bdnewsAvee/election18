<table class="display table table-bordered table-striped" id="load">
    <thead>
    <tr>
        <th>ID</th>
        <th>Album</th>
        <th>Title</th>
        <th>Path</th>
        <th>Description</th>
        <th>Sequence</th>
        <th>Featured</th>

        <th class="hidden-phone">Actions</th>
    </tr>


    </thead>
    <tbody>
    @foreach($video as $k=>$v)
        <tr class="item{{$v->id}}">
            <td>{{ $serial++ }}</td>
            <td>{{$v->albums->name}}</td>
            <td>{{$v->name}}</td>
            <td>
                <button class="show-modal btn btn-outline-primary" data-id="{{$v->id}}" data-content="{{$v->path}}">
                    <span class="fa fa-eye"></span> Show
                </button>
            </td>
            <td> {!! html_entity_decode($v->description) !!}  </td>
            <th>{{$v->sequence}}</th>
            <th>{{($v->is_featured == 0)? 'No':'Yes'}}</th>

            <td>
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"
                            aria-expanded="false">Actions <span class="caret"></span></button>
                    <ul role="menu" class="dropdown-menu float-right">
                        <li>
                            <a href="{{route("gallery_video.edit",$v->id)}}"><i class="fa fa-edit"></i> Edit</a>
                        </li>
                        <li>
                            <a class="delete-modal"  data-id="{{$v->id}}" data-title="{{$v->name}}" href="#"><i class="fa fa-trash-o"></i> Delete</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<ul class="pagination pagination-lg pull-right">{{$video->links()}}</ul>


<!-- Modal form to show a post -->
<div id="showModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div id="content_show" >

                </div>

            </div>
        </div>
    </div>
</div>


<!-- Modal form to delete a form -->
<div id="deleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <h3 class="text-center">Are you sure you want to delete the following post?</h3>
                <br/>
                <form class="form-horizontal" role="form">
                    <div class="form-group" style="display: none">
                        <label class="control-label col-sm-2" for="id">ID:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id_delete" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        {{--  <label class="control-label col-sm-2" for="title">Title:</label>--}}
                        <div class="col-sm-10">
                            <h3 id="title" class="text-center text-success"></h3>
                            <input type="hidden" class="form-control" id="title_delete" disabled>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                        <span id="" class='fa fa-trash-o'></span> Delete
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='fa fa-times'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>