@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
               Update   Video Gallery Information
            </header>
            <div class="panel-body">
                @include('layouts.backend._validationErrorMessages')
                {!! Form::model($editVideo,['route'=>['gallery_video.update',$editVideo->id],'method'=>'put','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}
                <div class="col-md-8 col-md-offset-2">
                    @include('backend.gallery.video._form')
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            {!! Form::submit('Update',['class'=>'btn btn-success btnCreate pull-right']) !!}
                            <a href="{{ route('gallery_video') }}" class="btn btn-warning pull-left"> <i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
</div>
@endsection
@push('css')
@endpush
@push('js')
    <!--this page  script only-->
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>
    @include('backend.gallery.video._script')
@endpush