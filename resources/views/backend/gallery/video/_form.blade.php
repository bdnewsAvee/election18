<div class="form-group">
    {!! Html::decode(  Form::label('gallery_albums_id','Albums <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">
        <div class="iconic-input right">

            {!! Form::select('gallery_albums_id',$albumsLists,null,['class'=>'form-control','required','placeholder'=>'Select Album','required']) !!}

        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('name','Title <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">
        <div class="iconic-input right">

            {!! Form::text('name',null,['class'=>'form-control','required','placeholder'=>'Type Video Title','required']) !!}

        </div>
    </div>
</div>
<div class="form-group">

    {!! Html::decode(  Form::label('path','Embed Code <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::textarea('path',null,['cols'=>5,'rows'=>2,'class'=>'form-control','required','placeholder'=>'Video Embed Code','required']) !!}

        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('sequence','Sequence ',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}

    <div class="col-lg-8">
        <div class="iconic-input right">

            {!! Form::select('sequence',$sequence,null,['class'=>'form-control','required','placeholder'=>'Select Sequence','required']) !!}

        </div>
    </div>
</div>
{{--<div class="form-group">
    {!! Html::decode(  Form::label('is_featured','Featured <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">

        <div class="iconic-input right">

            {!! Form::select('is_featured',['0'=>'No','1'=>'Yes'],null,['class'=>'form-control','required','required']) !!}

        </div>
    </div>
</div>--}}
<div class="form-group">
    {!! Html::decode(  Form::label('is_featured','Featured  ',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">

        <div class="iconic-input right" style="display: inline-flex;">

            <div class="radio">
                <label>
                    {!! Form::radio('is_featured',0,['class'=>'form-control','required','id'=>'optionsRadios2'])!!}

                    No
                </label>
            </div>
            <div class="radio">
                <label>
                    {!! Form::radio('is_featured',1,['class'=>'form-control','required','id'=>'optionsRadios1','checked'])!!}

                    Yes
                </label>
            </div>
        </div>

    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('description','Description',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::textarea('description',null,['cols'=>5,'rows'=>2,'class'=>'form-control ckeditor','placeholder'=>'Type Description']) !!}

        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('is_active','Status <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">

        <div class="iconic-input right" style="display: inline-flex;">
            <div class="radio">
                <label>
                    {!! Form::radio('is_active',0,['class'=>'form-control','required','id'=>'optionsRadios2'])!!}

                    Unpublished
                </label>
            </div>
            <div class="radio">
                <label>
                    {!! Form::radio('is_active',1,['class'=>'form-control','required','id'=>'optionsRadios1','checked'])!!}

                    Published
                </label>
            </div>


        </div>

    </div>
</div>