@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <header class="panel-heading">
                    Add Image Gallery Information
                </header>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    {!! Form::open(['route'=>'image-gallery','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}

                    <div class="col-md-8 col-md-offset-2">
                        @include("backend.gallery.image._form")
                        <div class="form-group">

                            <div class="col-md-8 col-md-offset-2">
                                {!! Form::submit('Save',['class'=>'btn btn-success btnCreate pull-right']) !!}
                                <a href="{{ route('gallery_image.index') }}" class="btn btn-warning pull-left"> <i
                                            class="fa fa-arrow-left"></i> Back</a>
                            </div>

                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            </section>
        </div>
    </div>
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.css')}}"/>
@endpush
@push('js')
    <script type="text/javascript" src="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>

    <!--this page  script only-->
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>

    @include('backend.gallery.image._script')
@endpush