@extends('layouts.backend.master')
@section('content')
{{--    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    Image Gallery
                </header>
                <div class="panel-body">
                    <div class="content">
                        <form action="{{ url('image-gallery') }}" class="form-image-upload" method="POST"
                              enctype="multipart/form-data">

                            {!! csrf_field() !!}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif
                        @include('backend.gallery.image._form')
                        </form>

                    </div>


                    <div class="row">
                        <div class='list-group gallery'>
                        @include('backend.gallery.image.table_load')
                        </div> <!-- list-group / end -->
                    </div> <!-- row / end -->

                </div>
            </section>

        </div>
    </div>--}} <!-- container / end -->
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Image Lists
            </header>
            <div class="panel-body">
                <div class="content">
                    <div class="row">
                        <div class="col-md-1">
                            <a href="{{route('gallery_image.create')}}" class="btn btn-success pull-left m-5"><i
                                        class="fa fa-plus"></i> Create</a>

                        </div>
                        {{ Form::open(['method'=>'get']) }}
                        <div class="col-md-3 col-md-offset-2">
                            {{ Form::text('name',null,['class'=>'form-control js-example-basic-single','placeholder'=>'Image Title']) }}
                        </div>

                        <div class="col-md-3 ">
                            {{ Form::select('gallery_albums_id',$albumsLists,null,['class'=>'form-control js-example-basic-single','placeholder'=>'Select Album']) }}
                        </div>

                        <div class="col-md-3">
                            {{ Form::button('<i class="fa fa-search"></i> Search',['type'=>'submit','class'=>'btn btn-primary']) }}
                            <a href="{{route('gallery_image.index')}}" class="btn btn-warning"><i
                                        class="fa fa-refresh"></i> Clear</a>
                        </div>

                        {{ Form::close() }}
                    </div>

                </div>
                <br>
                <div class="table-responsive userlistofadmin">
                    @if (count($images) > 0)
                        @include('backend.gallery.image.table_load')
                    @else
                        <h3 style="text-align: center">No Data Found</h3>
                    @endif
                </div>

            </div>
        </section>
    </div>
</div>
@endsection
@push('css')
    <link href="{{ asset('css/table-responsive.css') }}" rel="stylesheet"/>
    <style>
        iframe{width:100% !important;}
    </style>
@endpush
@push('js')
    @include('backend.gallery.image._script')
@endpush