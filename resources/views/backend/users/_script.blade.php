<script>
    // for ajax pagination without load
    $(function() {
        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault();

            $('#load a').css('color', '#dfecf6');
            $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/img/loading2.gif" />');

            var url = $(this).attr('href');
            getArticles(url);
            window.history.pushState("", "", url);
        });

        function getArticles(url) {
            $.ajax({
                url : url
            }).done(function (data) {
                $('.userlistofadmin').html(data);
            }).fail(function () {
                alert('Data could not be loaded.');
            });
        }
// password match validation message

        $('.btnCreate').click(function () {
            var password= $('.password').val();
            var cPassword= $('.cPassword').val();
            $('.passwordMessageDiv').remove();
            if(password != cPassword)
            {
                $('.passwordDiv').append('<i class="passwordMessageDiv red">Password mismatch</i>')
                event.preventDefault();
            }
        });

    });

</script>