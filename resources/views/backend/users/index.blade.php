@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    User Lists
                </header>
                <div class="panel-body">
                    <div class="content">
                        <div class="row">
                            {{ Form::open(['method'=>'get']) }}
                            <div class="col-md-3 col-md-offset-2">
                                {{ Form::text('name',null,['class'=>'form-control js-example-basic-single','placeholder'=>'Name']) }}
                            </div>

                            <div class="col-md-3 ">
                                {{ Form::text('email',null,['class'=>'form-control js-example-basic-single','placeholder'=>'Email Address']) }}
                            </div>

                            <div class="col-md-4">
                                {{ Form::button('<i class="fa fa-search"></i> Search',['type'=>'submit','class'=>'btn btn-primary']) }}

                             <a href="{{route('admin.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Create</a>
                            </div>


                            {{ Form::close() }}
                        </div>

                    </div>
                    <br>
                    <div class="table-responsive userlistofadmin">
                        @if (count($alltypeofuser) > 0)
                            @include('backend.users.table_load')
                        @else
                            <h3 style="text-align: center">No Data Found</h3>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@push('css')
    <link href="{{ asset('css/table-responsive.css') }}" rel="stylesheet"/>
@endpush
@push('js')

    @include('backend.users._script')
@endpush