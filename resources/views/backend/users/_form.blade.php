<div class="form-group">
    {!! Html::decode(  Form::label('name','Name <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8 ">
        <div class="iconic-input right">

            {!! Form::text('name',null,['class'=>'form-control','required','placeholder'=>'Type Name','required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('email','Email <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label'])) !!}
    <div class="col-md-8 ">
        <div class="iconic-input right">

            {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Type Email Address','required']) !!}
        </div>
    </div>
</div>


@if(!isset($admin))
    <div class="form-group">
        {!! Html::decode( Form::label('password','Password <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
        <div class="col-md-8  passwordDiv">
            <div class="iconic-input right">

                {!! Form::password('password',['class'=>'form-control password','required','placeholder'=>'Type Password']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Html::decode(  Form::label('password_confirmation','Confirm Password<sup><span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
        <div class="col-md-8  ">
            <div class="iconic-input right">

                {!! Form::password('password_confirmation',['class'=>'form-control cPassword','required','placeholder'=>'Re-enter Password']) !!}
            </div>

        </div>
    </div>
@endif