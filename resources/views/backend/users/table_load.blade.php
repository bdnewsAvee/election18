<table class="display table table-bordered table-striped" id="load">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th class="hidden-phone">Actions</th>
    </tr>


    </thead>
    <tbody>
    @foreach($alltypeofuser as $k=>$v)
        <tr>
            <td>{{ $serial++ }}</td>
            <td>{{$v->name}}</td>
            <td>{{$v->email}}</td>

            <td>
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"
                            aria-expanded="false">Actions <span class="caret"></span></button>
                    <ul role="menu" class="dropdown-menu float-right">
                        <li>
                            <a href="{{route("admin.edit",$v->id)}}"><i class="fa fa-edit"></i> Edit</a>
                        </li>
                        <li>
                            <a href="{{route("admin.delete",$v->id)}}"  onclick="return confirm('Are you sure to Delete This?')"><i class="fa fa-trash-o"></i> Delete</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<ul class="pagination pagination-lg pull-right">   {{$alltypeofuser->links()}}    </ul>