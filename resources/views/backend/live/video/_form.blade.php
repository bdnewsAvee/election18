<div class="form-group">
    {!! Html::decode(  Form::label('title','Title<sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">
        <div class="iconic-input right">

            {!! Form::text('title',null,['class'=>'form-control','required','placeholder'=>'Live Video Title','required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('description','Short Description <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label'])) !!}
    <div class="col-lg-8">
        <div class="iconic-input right">

            {!! Form::textarea('description',null,['cols'=>5,'rows'=>2,'class'=>'form-control','placeholder'=>'Short Description','required']) !!}
        </div>
    </div>
</div>


<div class="form-group">

    {!! Html::decode(  Form::label('path','Live Video Path <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::textarea('live_link',null,['cols'=>5,'rows'=>2,'class'=>'form-control ','required','placeholder'=>'Live Video Path','required']) !!}

        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('status','Status <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">

        <div class="iconic-input right" style="display: inline-flex;">
            <div class="radio">
                <label>
                    {!! Form::radio('status','Active',['class'=>'form-control','required','id'=>'optionsRadios1','checked'])!!}

                    Active
                </label>
            </div>
            <div class="radio">
                <label>
                    {!! Form::radio('status','Inactive',['class'=>'form-control','required','id'=>'optionsRadios2'])!!}

                    Inactive
                </label>
            </div>

        </div>

    </div>
</div>