@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    Update Live Video Information
                </header>
                <div class="panel-body">
                    @include('layouts.backend._validationErrorMessages')
                    {!! Form::model($liveLink,['route'=>['live_video.update',$liveLink->id],'method'=>'PUT','class'=>'form-horizontal' ]) !!}

                  <div class="col-md-8 col-md-offset-2">
                      @include('backend.live.video._form')
                      <div class="form-group">
                          <div class="col-md-8 col-md-offset-2">

                          {!! Form::submit('Update',['class'=>'btn btn-success btnCreate pull-right']) !!}
                              <a href="{{ route('home') }}" class="btn btn-warning pull-left"> <i class="fa fa-arrow-left"></i> Back</a>
                          </div>
                      </div>
                  </div>
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>
@endsection
@push('css')
@endpush
@push('js')
@endpush