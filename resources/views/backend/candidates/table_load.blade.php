
<table class="display table table-bordered table-striped" id="load">
    <thead>
    <tr>
        <th>ID</th>
        <th>প্রার্থীর  নাম</th>  
        <th>candidates name</th>  		
        <th>election_name(id)</th>
		<th>seat_name(id)</th>
        <th>party</th>  
        <th>symbol</th>
        <th>picture</th>  
        <th class="hidden-phone">Actions</th>
    </tr>


    </thead>
    <tbody>
 
    @foreach($candidates as $v)
        <tr>
            <td>{{$serial++}}</td>
			<td>{{  $v->candidates_name }}</td>
			<td>{{  $v->candidates_name_en }}</td>
		    <td>{{ $v->elect_nam }}</td>
			<td>{{ $v->constituency_name }}</td>
		    <td>{{ $v->candidates_party }}</td>
		    <td>{{ $v->candidates_symbol }}</td>
            <td>
             @if($v->candidates_photo)
  <img src="{{asset('candidates/images/'.$v->candidates_photo)}}" width="100" height="70" alt="{{ $v->candidates_photo }}">
                @else
                    Nan
                @endif
            </td>
		   
            <td>
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"
                            aria-expanded="false">Actions <span class="caret"></span></button>
                    <ul role="menu" class="dropdown-menu float-right">
                        <li>
                            <a href="{{route("candidates_manage.edit",$v->id)}}"><i class="fa fa-edit"></i> Edit</a>
                        </li>

                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<ul class="pagination pagination-lg pull-right">
{{--$candidates->onEachSide(5)->links()--}}

{{ $candidates->links() }}</ul>

