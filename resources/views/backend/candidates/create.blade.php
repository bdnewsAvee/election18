@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <header class="panel-heading">
                    Create Candidates
                </header>

                <div class="panel-body">
                    @include('layouts.backend._validationErrorMessages')
                    {!! Form::open(['route'=>'candidates_manage.store','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}
                    <div class="col-md-8 col-md-offset-2">
					
                        @include("backend.candidates._form")
                        <div class="form-group">

                            <div class="col-md-8 col-md-offset-2">
                                <a href="{{ route('candidates_manage.index') }}" class="btn btn-warning pull-right" onclick="return confirm('Are you confirm ?')"> <i class="fa fa-arrow-left"></i> Back</a>
                                {!! Form::submit('Save',['class'=>'btn btn-success btnCreate pull-left']) !!}
                            </div>

                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
              
            </div>
        </div>
    </div>
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.css')}}" />
@endpush
@push('js')
    <script type="text/javascript" src="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>

    @include('backend.candidates._script')
@endpush