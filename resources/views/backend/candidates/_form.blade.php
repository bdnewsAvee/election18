

<div class="form-group">
    {!! Html::decode(  Form::label('name','প্রার্থীর  নাম <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('candidates_name',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('name','candidates name<sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('candidates_name_en',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('election name','নির্বাচন নাম <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
        
{!! Form::select('candidates_election_id', $elections_table, old('candidates_election_id'), ['class'=> 'candidates_election_id','id'=>'candidates_election_id']) !!}		
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('path','আসনের নাম <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
        {!! Form::select('candidates_seat_id',$allseats) !!}        
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('election candidates party','ভোট প্রার্থীর দল <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('candidates_party',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('symbol','প্রতীক <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('candidates_symbol',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>
 

<div class="form-group">
    <label class="control-label col-md-2">Image Upload</label>
    <div class="col-md-8">
        <p style="color: red">Image Height: 400px</p>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                @if(isset($edit))
                    <img src="{{asset('candidates/images')}}/{{$edit->candidates_photo}}" alt=""/>
                @else
                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>

                @endif
            </div> 
            <div class="fileupload-preview fileupload-exists thumbnail"
                 style="max-width: 200px; max-height: 150px; line-height: 20px;">
		    </div>
			
            <div>
			   <span class="btn btn-white btn-file">
			   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
			   
			   
			   <span class="fileupload-exists"><i
						   class="fa fa-undo"></i> Change</span>
			   <input type="file" class="default" name="candidates_photo"/>
			   </span>
                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                            class="fa fa-trash"></i> Remove</a>
                           			
            </div>
        </div>
    </div>
</div>
