<table class="display table table-bordered table-striped" id="load">
    
    <thead>
    <tr>
        <th>ID</th>
        <th>constituency</th>     
        <th>seat_brief</th>    
        <th>election_name</th>
        <th>election_date</th>
        <!--th>total_center</th>  
        <th>total_vote</th>
        <th>male_vote</th>     
        <th>female_vote</th>    
        <th>vote_cast</th>
        <th>valid_vote</th>
        <th>invalid_vote</th>  
        <th>no_vote</th-->
        <th>winner_name</th>     
        <th>winning_party</th>    
        <th>winning_vote_count</th>
        <th>winner_symbol</th>
        <th>runner_up_name</th>  
        <th>runner_up_party</th>
        <th>runner_up_symbol</th>
        <th>runner_up_vote_count</th>
       
        <th class="hidden-phone">Actions</th>
    </tr>
    </thead>
	
    <tbody>
    @foreach($election_history as $k=>$v)
    <tr class="item{{$v->id}}">
    <td>{{ $serial++ }}</td>
    <td>{{  $v->constituency_id }}</td>
	<th>{{ $v->seat_brief }}</th>
	<th>{{ $v->elect_name }}</th>
	<th>{{ $v->election_date }}</th>
	<!--th>{{ $v->total_center }}</th>
	<th>{{ $v->total_vote }}</th>
	<th>{{ $v->male_vote }}</th>
	<th>{{ $v->female_vote }}</th>
	<th>{{ $v->vote_cast }}</th>
	<th>{{ $v->valid_vote }}</th>
	<th>{{ $v->invalid_vote }}</th>
	<th>{{ $v->no_vote }}</th-->
	<th>{{ $v->winner_name }}</th>
	<th>{{ $v->winning_party }}</th>
	<th>{{ $v->winning_vote_count }}</th>
	<th>{{ $v->winner_symbol }}</th>
	<th>{{ $v->runner_up_name }}</th>
	<th>{{ $v->runner_up_party }}</th>
	<th>{{ $v->runner_up_symbol }}</th>
	<th>{{ $v->runner_up_vote_count }}</th>
    <th>
		<div class="btn-group">
			<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"
					aria-expanded="false">Actions <span class="caret"></span></button>
			<ul role="menu" class="dropdown-menu float-right">
				<li>
					<a href="{{route("election_history.edit",$v->id)}}"><i class="fa fa-edit"></i> Edit</a>
				</li>

			</ul>
		</div>
	</th>

  @endforeach
    </tbody>
</table>

<ul class="pagination pagination-lg pull-right"> {{$election_history->links()}}   </ul>
