@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="  col-md-12">
        <section class="panel">
            <header class="panel-heading">
                Update Election History Information
            </header>
            <div class="panel-body">
                @include('layouts.backend._validationErrorMessages')
                {!! Form::model($edit,['route'=>['election_history.update',$edit->id],'method'=>'put','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}
                <div class="col-md-8 col-md-offset-2">
                @include('backend.election_history._form')
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            {!! Form::submit('Update',['class'=>'btn btn-success btnCreate pull-left']) !!}
                            <a href="{{ route('election_history.index') }}" class="btn btn-warning pull-right" onclick="return confirm('Are you confirm ?')"> <i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
</div>
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.css')}}" />
@endpush
@push('js')
    <script type="text/javascript" src="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
    <!--this page  script only-->
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>
    @include('backend.election_history._script')
@endpush