

<div class="form-group">
    {!! Html::decode(  Form::label('path','আসনের নাম <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
        {!! Form::select('constituency_id',$allseats) !!}        
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('seat brief','সংক্ষিপ্ত বিবরণ <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::textarea('seat_brief',null,['cols'=>5,'rows'=>2,'class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('election name','নির্বাচন নাম <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
       <div class="iconic-input right">  

{!! Form::select('election_name', $elections_table, old('election_name'), ['class'=> 'election_name','id'=>'election_name']) !!}
		   
        </div>
    </div>
</div>


<div class="form-group">
    {!! Html::decode(  Form::label('election date','নির্বাচনী তারিখ <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('election_date',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('total center','মোট কেন্দ্র <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('total_center',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>
 
<div class="form-group">
    {!! Html::decode(  Form::label('total vote','total_vote <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('total_vote',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('male vote','male_vote <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('male_vote',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('female vote','female_vote <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('female_vote',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('vote cast','vote_cast <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('vote_cast',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('valid vote','valid_vote <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('valid_vote',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('invalid vote','invalid_vote <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('invalid_vote',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('no vote','no_vote <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('no_vote',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('winner name','winner_name <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('winner_name',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('winning party','winning_party <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('winning_party',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('winning_vote_count','winning_vote_count <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('winning_vote_count',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

 
 <div class="form-group">
    {!! Html::decode(  Form::label('winner_symbol','winner_symbol <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('winner_symbol',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('runner_up_name','runner_up_name <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('runner_up_name',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('runner_up_party','runner_up_party <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('runner_up_party',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('runner_up_symbol','runner_up_symbol <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('runner_up_symbol',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('runner_up_vote_count','runner_up_vote_count <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::text('runner_up_vote_count',null,['class'=>'form-control','placeholder'=>'Write here']) !!}
        </div>
    </div>
</div>


