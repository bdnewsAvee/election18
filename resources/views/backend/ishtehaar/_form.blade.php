<div class="form-group">
    {!! Html::decode(  Form::label('political_parties_id','Parties <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}

    <div class="col-lg-8">
        <div class="iconic-input right">

            {!! Form::select('political_parties_id',$party,null,['class'=>'form-control','required','placeholder'=>'Select Party']) !!}

        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('title','Title ',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">
        <div class="iconic-input right">
            {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Type Title']) !!}
        </div>
    </div>
</div>


{{--<div class="form-group">
    {!! Html::decode(  Form::label('image','Image <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">
        <div class="iconic-input right">
            {!! Form::file('image',null,['class'=>'form-control','required','placeholder'=>'Type Title','required']) !!}
        </div>
    </div>
</div>--}}

<div class="form-group">
    <label class="control-label col-md-2">Image Upload</label>
    <div class="col-md-8">
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                @if(isset($editAIshtehaar))
                    <img src="{{asset('electionmanifesto')}}/{{$editAIshtehaar->image}}" alt=""/>
                @else
                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>

                @endif
            </div>
            <div class="fileupload-preview fileupload-exists thumbnail"
                 style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                                                   <span class="btn btn-white btn-file">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                   <span class="fileupload-exists"><i
                                                               class="fa fa-undo"></i> Change</span>
                                                   <input type="file" class="default" accept=".jpg,.png,.jpge" name="image"/>
                                                   </span>
                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                            class="fa fa-trash"></i> Remove</a>
            </div>
        </div>
    </div>
</div>


<div class="form-group">
    {!! Html::decode(  Form::label('file_path','File <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">
        <div class="iconic-input right">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input name="file_path" type="file" accept=".pdf" class="default"/>
                                                </span>
                <span class="fileupload-preview" style="margin-left:5px;"></span>
                <a href="#" class="close fileupload-exists" data-dismiss="fileupload"
                   style="float: none; margin-left:5px;"></a>
            </div>
            {{--  {!! Form::file('file_path',null,['class'=>'form-control','required','placeholder'=>'Select File']) !!}--}}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('short_details','Description ',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">
            {!! Form::textarea('short_details',null,['cols'=>5,'rows'=>2,'class'=>'form-control ckeditor','placeholder'=>'Description']) !!}

        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('is_active','Status <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-lg-8">

        <div class="iconic-input right" style="display: inline-flex;">

            <div class="radio">
                <label>
                    {!! Form::radio('is_active',1,['class'=>'form-control','required','id'=>'optionsRadios2'])!!}

                    Active
                </label>
            </div>
            <div class="radio">
                <label>
                    {!! Form::radio('is_active',0,['class'=>'form-control','required','id'=>'optionsRadios1','checked'])!!}

                   Inactive
                </label>
            </div>

        </div>

    </div>
</div>
