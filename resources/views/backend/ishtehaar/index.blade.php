@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Ishtehaar Lists
                </header>
                <div class="panel-body">
                    <div class="content">
                        <div class="row">
                            <div class="col-md-1">
                                <a href="{{route('ishtehaar_manage.create')}}" class="btn btn-success pull-left m-5"><i
                                            class="fa fa-plus"></i> Create</a>

                            </div>

                            {{ Form::open(['method'=>'get']) }}
                            <div class="col-md-3 col-md-offset-2">
                                {{ Form::select('party_name',$party,null,['class'=>'form-control js-example-basic-single','placeholder'=>'Select Party Name']) }}
                            </div>

                            <div class="col-md-3">
                                {{ Form::button('<i class="fa fa-search"></i> Search',['type'=>'submit','class'=>'btn btn-primary']) }}
                                <a href="{{route('ishtehaar_manage.index')}}" class="btn btn-warning"><i
                                            class="fa fa-refresh"></i> Clear</a>
                            </div>

                            {{ Form::close() }}

                        </div>

                    </div>
                    <br>
                    <div class="table-responsive userlistofadmin">
                        @if (count($allishtehaar) > 0)
                            @include('backend.ishtehaar.table_load')
                        @else
                            <h3 style="text-align: center">No Data Found</h3>
                        @endif
                    </div>

                </div>
            </section>
        </div>
    </div>

@endsection
@push('css')
    <link href="{{ asset('css/table-responsive.css') }}" rel="stylesheet"/>
    <style>
        iframe {
            width: 100% !important;
        }
    </style>
@endpush
@push('js')
    @include('backend.ishtehaar._script')
@endpush