@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                Update Ishtehaar Information
            </header>
            <div class="panel-body">
                @include('layouts.backend._validationErrorMessages')
                {!! Form::model($editAIshtehaar,['route'=>['ishtehaar_manage.update',$editAIshtehaar->id],'method'=>'put','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}
                <div class="col-md-8 col-md-offset-2">
                    @include('backend.ishtehaar._form')
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            {!! Form::submit('Update',['class'=>'btn btn-success btnCreate pull-right']) !!}
                            <a href="{{ route('ishtehaar_manage.index') }}" class="btn btn-warning pull-left"> <i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </section>
    </div>
</div>
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.css')}}" />

@endpush
@push('js')
    <script type="text/javascript" src="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>

    <!--this page  script only-->
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>
    @include('backend.ishtehaar._script')
@endpush