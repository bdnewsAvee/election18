<div class="form-group">
    {!! Html::decode(  Form::label('api_url','Api Url <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label']) )!!}
    <div class="col-md-8">
        <div class="iconic-input right">

            {!! Form::text('api_url',null,['class'=>'form-control','required','placeholder'=>'Api Url','required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('api_data','Api Data <sup> <span class="red">*</span></sup>',['class'=>'col-lg-2 col-sm-2 control-label'])) !!}
    <div class="col-md-8">
        <div class="iconic-input right">

            {!! Form::textarea('api_data',null,['class'=>'form-control','placeholder'=>'Api Data','required']) !!}
        </div>
    </div>
</div>

