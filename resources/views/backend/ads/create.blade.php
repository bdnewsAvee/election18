@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <header class="panel-heading">
                 Create  Adds
                </header>
                <div class="panel-body">
                    @include('layouts.backend._validationErrorMessages')
                    {!! Form::open(['route'=>'ads_manage.store','class'=>'form-horizontal','enctype'=>"multipart/form-data",'id'=>'userform']) !!}

                    <div class="col-md-8 col-md-offset-2">
                         @include("backend.ads._form")
                        <div class="form-group">

                            <div class="col-md-8 col-md-offset-2">
                                {!! Form::submit('Save',['class'=>'btn btn-success btnCreate pull-right']) !!}
                                <a href="{{ route('ads_manage.index') }}" class="btn btn-warning pull-left"> <i
                                            class="fa fa-arrow-left"></i> Back</a>
                            </div>

                        </div>
                    </div>
                        {!! Form::close() !!}


                </div>
            </div>
            </section>
        </div>
    </div>
@endsection
@push('css')
@endpush
@push('js')
    <!--this page  script only-->
    <script src="{{ asset('js/advanced-form-components.js') }}"></script>
    @include('backend.ads._script')
@endpush