
<div class="form-group">

    {!! Html::decode(  Form::label('lg','JS Code For Large Screen  <sup> <span class="red">*</span></sup>',['class'=>'col-md-3 control-label']) )!!}
    <div class="col-md-7">
        <div class="iconic-input right">
            {!! Form::textarea('js_code_lg',null,['id'=>'lg','cols'=>5,'rows'=>2,' class'=>'form-control','placeholder'=>'JS Code For Large Screen ']) !!}

        </div>
    </div>
</div>
<div class="form-group">
    {!! Html::decode(  Form::label('md','JS Code For Medium Screen <sup> <span class="red">*</span></sup>',['class'=>'col-md-3 control-label']) )!!}
    <div class="col-md-7">
        <div class="iconic-input right">
            {!! Form::textarea('js_code_md',null,['id'=>'md','cols'=>5,'rows'=>2,'class'=>'form-control','placeholder'=>'JS Code For Medium Screen']) !!}

        </div>
    </div>
</div>
<div class="form-group">

    {!! Html::decode(  Form::label('xs','JS Code For Mobile Screen  <sup> <span class="red">*</span></sup>',['class'=>'col-md-3 control-label']) )!!}
    <div class="col-md-7">
        <div class="iconic-input right">
            {!! Form::textarea('js_code_xs',null,['id'=>'xs','cols'=>5,'rows'=>2,'class'=>'form-control','placeholder'=>'JS Code For Mobile Screen']) !!}

        </div>
    </div>
</div>


<div class="form-group">
    {!! Html::decode(  Form::label('ads_code','Ads Code <sup> <span class="red">*</span></sup>',['class'=>'col-md-3 control-label']) )!!}
    <div class="col-md-7">
        <div class="iconic-input right">
            {!! Form::textarea('ads_code',null,['cols'=>5,'rows'=>4,'class'=>'form-control','placeholder'=>'Ads Code']) !!}

        </div>
    </div>
</div>

<div class="form-group">
    {!! Html::decode(  Form::label('is_active','Status <sup> <span class="red">*</span></sup>',['class'=>'col-md-3 control-label']) )!!}
    <div class="col-lg-8">

        <div class="iconic-input right" style="display: inline-flex;">
            <div class="radio">
                <label>
                    {!! Form::radio('status','Active',['class'=>'form-control','required','id'=>'optionsRadios1','checked'])!!}

                    Active
                </label>
            </div>
            <div class="radio">
                <label>
                    {!! Form::radio('status','Inactive',['class'=>'form-control','required','id'=>'optionsRadios2'])!!}

                    Inactive
                </label>
            </div>

        </div>

    </div>
</div>
