@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Ads Lists
                </header>
                <div class="panel-body">
                    <div class="content">
                        <div class="row">
                            {{ Form::open(['method'=>'get']) }}
                            <div class="col-md-3 col-md-offset-2">
{{--                                {{ Form::select('position_name',$sequence,null,['class'=>'form-control js-example-basic-single','placeholder'=>'Ads Position Name']) }}--}}
                                {!! Form::select('search',['Universal'=>'Universal Ads','Home'=>'Home Page Ads','Image'=>'Image Gallery Page Ads','Video'=>'Video Gallery Page Ads','Manifesto'=>'Manifesto Page Ads','Seat'=>'Seat Page Ads','Candidate'=>'Candidate Page Ads'],\Illuminate\Support\Facades\Input::get('search'),[ 'class'=>'form-control form-search',
                                      'placeholder'=>'Please select']) !!}
                            </div>

                            <div class="col-md-3">
                                {{ Form::button('<i class="fa fa-search"></i> Search',['type'=>'submit','class'=>'btn btn-primary']) }}
                                <a href="{{route('ads_manage.index')}}" class="btn btn-info"><i class="fa fa-refresh"></i> Refresh</a>
                            </div>

                            {{ Form::close() }}
                        </div>

                    </div>
                    <br>
                    <div class="table-responsive userlistofadmin">
                        @if (count($ads) > 0)
                            @include('backend.ads.table_load')
                        @else
                            <h3 style="text-align: center">No Data Found</h3>
                        @endif
                    </div>

                </div>
            </section>
        </div>
    </div>

@endsection
@push('css')
    <link href="{{ asset('css/table-responsive.css') }}" rel="stylesheet"/>
@endpush
@push('js')
    @include('backend.ads._script')
@endpush