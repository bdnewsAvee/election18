<table class="display table table-bordered table-striped" id="load">
    <thead>
    <tr>
        <th style="width: 5%">ID</th>
        <th style="width: 25%">Ads Position</th>
        <th style="width: 50%">Codes</th>
        <th style="width: 10%">Status</th>
        <th style="width: 10%">Actions</th>
    </tr>


    </thead>
    <tbody>
    @foreach($ads as $k=>$v)
        <tr>
            <td>{{ $serial++ }}</td>
            <td>
                <p>{{$v->page}}</p>
                {{$v->position_name}}
            </td>
            <td>
                <p><b>Large Screen - </b>{{ $v->js_code_lg }}</p>
                <p><b>Medium Screen - </b>{{ $v->js_code_md }}</p>
                <p><b>Small Screen - </b>{{ $v->js_code_sm }}</p>
                <p>{{ $v->ads_code }}</p>
            </td>


            <th>{{($v->status == 'Active')? 'Active':'Inactive'}}</th>

            <td>
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"
                            aria-expanded="false">Actions <span class="caret"></span></button>
                    <ul role="menu" class="dropdown-menu float-right">
                        <li>
                            <a href="{{route("ads_manage.edit",$v->id)}}"><i class="fa fa-edit"></i> Edit</a>
                        </li>
                 {{--       <li>
                            <a href="{{route("ads_manage.delete",$v->id)}}"><i class="fa fa-trash-o"></i> Delete</a>
                        </li>--}}
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<ul class="pagination pagination-lg pull-right">{{$ads->links()}}</ul>

