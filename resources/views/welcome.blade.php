@extends('layouts.frontend.master')

@section('content')
    @if(isset($slideData))
        <section id="main-slide" style="padding-top:10px;">
            <div class="container">


                <div class="row">

                    <div class="col-md-8 padding-0 padding-right-5">

                        <div class="row visible-xs padding-bottom-10px">
                            <div class="col-xs-12">
                                <div class="section-title">
                                    <h3>মানচিত্রে ভোটচিত্র </h3>
                                </div>
                                <div class="map" style="overflow:hidden;border:1px solid #f0f0f0;background: url({{ asset('frontend/images/loading.gif')}}) center center no-repeat">
                                    <iframe class="map-frame" src='{{ asset('map') }}'></iframe>
                                </div>
                            </div>

                        </div>
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            {{--<!-- Indicators -->--}}
                            {{--<ol class="carousel-indicators">--}}
                                {{--@foreach($slideData as $k=>$slide)--}}
                                    {{--<li data-target="#carousel-example-generic" data-slide-to="{{$k}}"--}}
                                        {{--class="{{ ($k==0)? 'active':''}}"></li>--}}
                                {{--@endforeach--}}
                            {{--</ol>--}}

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">

                                @foreach($slideData as $k=>$slide)
                                    <div class="item {{ ($k==0)? 'active':''}}">
                                        <img src="{{str_replace('/BINARY/','/ALTERNATES/w940/',$slide->image) }}"
                                             alt="{{$slide->title}}">
                                        <div class="carousel-caption" >
                                            <h4><a style="color: white ;" href="{{route('news.details',$slide->id)}}">{{$slide->title}}</a></h4>

                                        </div>
                                    </div>
                                @endforeach

                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button"
                               data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button"
                               data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    </div>
                    <div class="col-md-4  padding-0 padding-left-5">
                        <div class="row">
                            <div class="col-md-12">
                                @if(!empty($liveLink))
                                    <div class="embed-responsive embed-responsive-16by9"
                                         style="background: #4F4E4E; height: 100%;">
                                        {!! html_entity_decode($liveLink->live_link) !!}
                                    </div>
                                    <div style="background: #4F4E4E;padding:11px 15px 12px 15px;color:#e0e0e0;">
                                        <h3 class="text-left" style="margin:0;line-height:20px;font-size:20px;">
                                            <span style="color:#FF5757;display:block;margin-bottom:5px;">
                                                <i class="fas fa-circle" style="color:#FF5757"></i>&nbsp; লাইভ
                                            </span>
                                            <span style="display:block;margin-bottom:5px;">
                                                {!! $liveLink->title!!}
                                            </span>
                                        </h3>
                                        <p style="margin: 0px">{!! $liveLink->description !!}</p>
                                    </div>
                                    @if(isset($ads['Home']) && isset($ads['Home']['UNDER-LIVE']))
                                        <div style="padding-top: 10px;width: 100%;height: 150px;overflow: hidden">
                                            {!! html_entity_decode($ads['Home']['UNDER-LIVE']->ads_code) !!}
                                        </div>
                                    @endif
                                @else
                                    @if(isset($ads['Home']) && isset($ads['Home']['SLIDER-RIGHT']))
                                        <div style="height: 468px;">
                                            {!! html_entity_decode($ads['Home']['SLIDER-RIGHT']->ads_code) !!}
                                        </div>
                                    @endif
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    @endif

    <section id="add" class="add-wrapper m-top-15">
        <div class="container">
            @if(isset($ads['Home']) && isset($ads['Home']['UNDER-SLIDER']))
                <div class="row">
                    <div class="col-md-12">
                        <div class="content-top-add-banner">
                            {!! html_entity_decode($ads['Home']['UNDER-SLIDER']->ads_code) !!}
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
    <section id="map-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 padding-0 padding-right-5">
                    <div class="row hidden-xs">
                        <div class="col-md-12">
                            <div class="section-title">
                                <h3>মানচিত্রে ভোটচিত্র </h3>
                            </div>
                            <div class="map" style="overflow:hidden;border:1px solid #f0f0f0;background: url({{ asset('frontend/images/loading.gif')}}) center center no-repeat">
                                <iframe class="map-frame" src="{{ asset('map') }}"></iframe>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        @if(isset($ALlNews0ne) || isset($ALlNewsTwo))
                        <div class="col-md-12">
                            <div class="section-title">
                                <h3>ভোটের খবর</h3>
                            </div>
                        </div>
                        @endif
                            @if(isset($ALlNews0ne))
                        <div class="col-md-12">

                                <ul class="all-news">
                                    @php $i = 0; @endphp
                                    @foreach($ALlNews0ne as $k=>$newone)
                                        @php $i++ @endphp

                                        <li class="col-md-6">
                                            <ul class="news-item-rk row itm-{{$i}}">
                                                <li style="float:left;width:80px;">
                                                    <a href=""> <img class=""
                                                                     src="{{ str_replace('/BINARY/','/ALTERNATES/w185/',$newone->image) }}"
                                                                     alt="{{ $newone->title }}" width="60"></a>
                                                </li>
                                                <li style="float:left; width: 70%">
                                                    <a href="{{route('news.details',$newone->id)}}" class="news-title-home">{{ $newone->title }}</a>
                                                    <p class="teaser" style="display: inline;">{{$newone->teaser}}</p>
                                                    <p class="news-time"></p>
                                                </li>
                                            </ul>
                                        </li>
                                        @if($i > 1 && $i%2 == 0)
                                            <div class="clearfix"></div>
                                        @endif

                                    @endforeach
                                    <li style="clear: both !important;"></li>
                                </ul>
                        </div>

                            @endif
                    </div>

                    @if(isset($ads['Home']) && isset($ads['Home']['ELECTION-NEWS-MIDDLE']))
                        <div class="row add-wrapper">
                            <div class="col-md-12 ">
                                <div class="content-top-add-banner">
                                    {!! html_entity_decode($ads['Home']['ELECTION-NEWS-MIDDLE']->ads_code) !!}
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class=" m-top-15 row">
                        <div class="col-md-12">
                            @if(isset($ALlNewsTwo))
                                <ul class="all-news">
                                    @php $i = 0; @endphp
                                    @foreach($ALlNewsTwo as $k=>$newtwo)
                                        @php $i++ @endphp
                                        <li class="col-md-6">
                                            <ul class="news-item-rk row">
                                                <li style="float:left;width:80px;">
                                                    <a href=""> <img class=""
                                                                     src="{{ str_replace('/BINARY/','/ALTERNATES/w185/',$newtwo->image) }}"
                                                                     alt="{{ $newtwo->title }}" width="60"></a>
                                                </li>
                                                <li style="float:left; width: 70%">
                                                    <a href="{{route('news.details',$newtwo->id)}}" class="news-title-home" style="display: block;" >{{ $newtwo->title }}</a>
                                                    <p class="teaser" style="display: inline;">{{$newtwo->teaser}}</p>
                                                    <p class="news-time"></p>
                                                </li>
                                            </ul>
                                        </li>
                                        @if($i > 1 && $i%2 == 0)
                                            <div class="clearfix"></div>
                                        @endif


                                    @endforeach
                                    <li style="clear: both !important;"></li>
                                </ul>
                            @endif
                        </div>

                    </div>

                </div>

                <div class="col-md-4 padding-0 padding-left-5">
                    @include('layouts.frontend._right-aside')
                </div>
            </div>
        </div>
    </section>
    @if(isset($video) && count($video)>0)
        <section id="feature-video" class="feature-video">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title">
                            <h3>ভিডিও
                                <a href="" class="pull-right customNextBtn"><i class="fas fa-chevron-right" style="color:#e0e0e0;"></i></a>
                                <a href="" class="pull-right customPrevBtn"><i class="fas fa-chevron-left" style="color:#e0e0e0;"></i></a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="owl-carousel videoCarousel ">
                            @foreach($video as $k=>$v)
                                <div>
                                    {!! html_entity_decode($v->path) !!}
                                </div>
                            @endforeach
                        </div>


                    </div>
                </div>
            </div>
        </section>
    @endif


        <section id="opinion" style="margin-bottom:30px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 padding-0 padding-right-5">
                    @if(isset($opinion) &&  count($opinion)>0)
                        <!--=== VOTE MOTAMOT === -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title">
                                    <h3>ভোট মতামত</h3>
                                </div>
                            </div>
                            <div class="col-md-12 padding-0">
                                <div class="col-md-6 opinion-left">

                                    <a href="#"> <img class="img-responsive" src="{{ str_replace('/BINARY/','/ALTERNATES/w316/',$opinion[0]['image_url']) }}"></a>
                                </div>

                                <div class="col-md-6 opinion-right">
                                    <ul>
                                        @foreach($opinion as $k=>$item)
                                            <li>
                                                <a href="{{$item->opinion_details_link}}" class="news-title-home" style="font-weight:900;" target="_blank">{{$item->title}}</a>
                                                <p class="">{{$item->short_details}}</p>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                        <!--=== CHOBI GHOR === -->

                        @if(isset($image) &&  count($image)>0)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title">
                                        <h3>ছবিঘর</h3>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="carousel-photo-generic" class="carousel slide" data-ride="carousel">

                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">

                                            @foreach($image as $k=>$imageval)

                                                <div class="item {{ ($k==0)? 'active':''}}">
                                                    <img src="{{ asset('gallery/images') }}/{{$imageval->path}}"
                                                         alt="{{$imageval->name}}" class="img-400px">
                                                    <div class="carousel-caption">
                                                        <h4>{{$imageval->name}}</h4>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-photo-generic" role="button"
                                           data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-photo-generic" role="button"
                                           data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        @endif
                    </div>
                    <!--=== RIGHT PAN === -->
                    <div class="col-md-4 padding-0 padding-left-5">
                        @if(isset($ads['Home']) && isset($ads['Home']['VOTE-OPINION-RIGHT-1ST']))
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="right-add-banner">
                                        {!! html_entity_decode($ads['Home']['VOTE-OPINION-RIGHT-1ST']->ads_code) !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(isset($ads['Home']) && isset($ads['Home']['VOTE-OPINION-RIGHT-2ND']))
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="right-add-banner">
                                        {!! html_entity_decode($ads['Home']['VOTE-OPINION-RIGHT-2ND']->ads_code) !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>


@endsection