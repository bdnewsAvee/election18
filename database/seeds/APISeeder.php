<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class APISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('api_manages')->insert([

            [
                'id'=>1,
                'api_url'=> 'https://bangla.bdnews24.com/json/v0_01/adn/news-list.jsp?token=adn91$D1G1yH',
                'api_data'=> 'null',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
    ]);
    }
}
