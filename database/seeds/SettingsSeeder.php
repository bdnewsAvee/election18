<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([

            [
                'id'=>1,
                'key'=> 'CURRENT_ELECTION',
                'value'=> '11',
                'extra'=> null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
    ]);
    }
}
