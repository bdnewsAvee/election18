<?php

use App\Divisions;
use Illuminate\Database\Seeder;

class DivisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $division_data = [
            ['Chattogram','চট্টগ্রাম'],
            ['Barishal','বরিশাল'],
            ['Khulna','খুলনা'],
            ['Rajshahi','রাজশাহী'],
            ['Rangpur','রংপুর'],
            ['Sylhet','সিলেট'],
            ['Mymensingh','ময়মনসিংহ'],
            ['Dhaka','ঢাকা']
        ];
        foreach($division_data as $division)
        {
            Divisions::create(array(
                'name' => $division[0],
                'bn_name' => $division[1],
            ));
        }
    }
}
