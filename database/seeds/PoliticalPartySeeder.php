<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class PoliticalPartySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('political_parties')->insert([

        [ 'id'=>1,  'name'=>'বাংলাদেশ আওয়ামী লীগ',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>2,  'name'=>'বাংলাদেশ জাতীয়তাবাদী দল',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>3,  'name'=>'জাতীয় পার্টি (এরশাদ)',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>4,  'name'=>'লিবারেল ডেমোক্র্যাটিক পার্টি (বাংলাদেশ)',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>5,  'name'=>'জাতীয় পার্টি (মঞ্জু)',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>6,  'name'=>'বাংলাদেশের কমিউনিস্ট পার্টি (মার্কসবাদী-লেনিনবাদী) (বড়ুয়া)',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>7,  'name'=>'কৃষক শ্রমিক জনতা লীগ',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>8,  'name'=>'বাংলাদেশ কমিউনিস্ট পার্টি',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>9,  'name'=>'জাতীয় আওয়ামী লীগ',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>10, 'name'=> 'বাংলাদেশ ওয়ার্কার্স পার্টি',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>11, 'name'=> 'বিকাল্পা ধর বাংলাদেশ',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>12, 'name'=> 'জাতীয় সমাজতান্ত্রিক দল ',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>13, 'name'=> 'জাতীয় সমাজতান্ত্রিক দল - জেএসডি',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>15, 'name'=> 'বাংলাদেশ জাতীয় পার্টি - বিজেপি',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>16, 'name'=> 'বাংলাদেশ খিলাফত আন্দোলন',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>17, 'name'=> 'গানো ফোরাম',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>18, 'name'=> 'প্রগ্রেসিভ ডেমোক্র্যাটিক পার্টি (বাংলাদেশ)',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>19, 'name'=> 'বাংলাদেশ জাতীয় পার্টি',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>20, 'name'=> 'ইসলামী ফ্রন্ট বাংলাদেশ',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>21, 'name'=> 'ইসলামী ঐক্য জোট',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>22, 'name'=> 'বাংলাদেশ খেলাফত মজলিশ',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>24, 'name'=> 'ইসলামী আন্দোলন বাংলাদেশ',  'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
        [ 'id'=>25, 'name'=> 'বাংলাদেশ ইসলামী ফ্রন্ট' ,'logo'=> null,'description'=> null,'sequence'=> null, 'color'=> null ,'status'=> 'Active', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],

    ]);
    }
}
