<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class LiveVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('live_videos')->insert([

            [
                'id'=>1,
                'title'=> null,
                'description'=> null,
                'live_link'=> null,
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
    ]);
    }
}
