<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads')->insert([

            [
                'id'=>1,
                'page'=> 'Universal',
                'position_name'=> 'TOP',
                'js_code_lg'=> 'null',
                'js_code_md'=> 'null',
                'ads_code'=> 'null',
                'js_code_xs'=> 'null',
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id'=>2,
                'page'=> 'Universal',
                'position_name'=> 'RIGHT-SIDE-UP-WINDOW-SECTION',
                'js_code_lg'=> 'null',
                'js_code_md'=> 'null',
                'ads_code'=> 'null',
                'js_code_xs'=> 'null',
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id'=>3,
                'page'=> 'Universal',
                'position_name'=> 'RIGHT-SIDE-DOWN-WINDOW-SECTION',
                'js_code_lg'=> 'null',
                'js_code_md'=> 'null',
                'ads_code'=> 'null',
                'js_code_xs'=> 'null',
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id'=>4,
                'page'=> 'Home',
                'position_name'=> 'UNDER-LIVE',
                'js_code_lg'=> 'null',
                'js_code_md'=> 'null',
                'ads_code'=> 'null',
                'js_code_xs'=> 'null',
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id'=>5,
                'page'=> 'Home',
                'position_name'=> 'SLIDER-RIGHT',
                'js_code_lg'=> 'null',
                'js_code_md'=> 'null',
                'ads_code'=> 'null',
                'js_code_xs'=> 'null',
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id'=>6,
                'page'=> 'Home',
                'position_name'=> 'UNDER-SLIDER',
                'js_code_lg'=> 'null',
                'js_code_md'=> 'null',
                'ads_code'=> 'null',
                'js_code_xs'=> 'null',
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id'=>7,
                'page'=> 'Home',
                'position_name'=> 'ELECTION-NEWS-MIDDLE',
                'js_code_lg'=> 'null',
                'js_code_md'=> 'null',
                'ads_code'=> 'null',
                'js_code_xs'=> 'null',
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id'=>8,
                'page'=> 'Home',
                'position_name'=> 'VOTE-OPINION-RIGHT-1ST',
                'js_code_lg'=> 'null',
                'js_code_md'=> 'null',
                'ads_code'=> 'null',
                'js_code_xs'=> 'null',
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id'=>9,
                'page'=> 'Home',
                'position_name'=> 'VOTE-OPINION-RIGHT-2ND',
                'js_code_lg'=> 'null',
                'js_code_md'=> 'null',
                'ads_code'=> 'null',
                'js_code_xs'=> 'null',
                'status'=> 'Inactive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],

    ]);
    }
}
