<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiManagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_manages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('api_url');
            $table->json('api_data')->nullable();
            $table->timestamps();
        });
        Artisan::call('db:seed', [
            '--class' => APISeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_manages');
    }
}
