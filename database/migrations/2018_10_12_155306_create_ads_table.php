<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('page',['Universal','Home','Image','Video','Manifesto','Seat','Candidate']);
            $table->string('position_name')->nullable();
            $table->string('js_code_lg')->nullable();
            $table->string('js_code_md')->nullable();
            $table->string('js_code_xs')->nullable();
            $table->longText('ads_code')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
        Artisan::call('db:seed', [
            '--class' => AdsTableSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
