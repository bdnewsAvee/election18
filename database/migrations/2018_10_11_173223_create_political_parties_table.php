<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliticalPartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('political_parties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('logo')->nullable();
            $table->text('description')->nullable();
            $table->integer('sequence')->nullable();
            $table->string('color')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->softDeletes();
            $table->unsignedSmallInteger('created_by')->default(1);
            $table->unsignedSmallInteger('updated_by')->nullable();
            $table->timestamps();
        });
        Artisan::call('db:seed', [
            '--class' => PoliticalPartySeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('political_parties');
    }
}
