<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectionManifestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('election_manifesto', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('political_parties_id');
            $table->foreign('political_parties_id')->references('id')->on('political_parties')->onUpdate('cascade')->onDelete('restrict');
            $table->string('title');
            $table->mediumText('short_details')->nullable();
            $table->string('image');
            $table->string('file_path');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('election_manifesto');
    }
}
