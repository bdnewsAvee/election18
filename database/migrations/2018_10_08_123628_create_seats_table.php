<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('division_id');
            $table->integer('district_id');
            $table->string('name')->nullable();
            $table->string('name_bn');
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->integer('constituency')->nullable();
            $table->string('x_coordinate')->nullable();
            $table->string('y_coordinate')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->softDeletes();
            $table->unsignedSmallInteger('created_by')->default(1);
            $table->unsignedSmallInteger('updated_by')->nullable();
            $table->timestamps();
        });
        Artisan::call('db:seed', [
            '--class' => SeatsSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seats');
    }
}
