<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllelectionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allelection_histories', function (Blueprint $table) {
            $table->increments('id');
			$table->string('constituency_id');
			$table->string('seat_brief')->nullable();
			$table->string('election_name');
            $table->string('election_date')->nullable();
            $table->integer('total_center')->nullable();
			$table->integer('total_vote')->nullable();
			$table->integer('male_vote')->nullable();
			$table->integer('female_vote')->nullable();
			$table->integer('vote_cast')->nullable();
            $table->integer('valid_vote')->nullable();
            $table->integer('invalid_vote')->nullable();
			$table->integer('no_vote')->nullable();
			$table->string('winner_name')->nullable();
			$table->string('winning_party')->nullable();
			$table->integer('winning_vote_count')->nullable();
            $table->string('winner_symbol')->nullable();
            $table->string('runner_up_name')->nullable();
			$table->string('runner_up_party')->nullable();
			$table->string('runner_up_symbol')->nullable();
			$table->integer('runner_up_vote_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allelection_histories');
    }
}
