<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpinionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->mediumText('image_url');
            $table->integer('sequence')->nullable();
            $table->longText('opinion_details_link')->nullable();
            $table->text('short_details')->nullable();
            $table->enum('status',['Published','Unpublished'])->default('Unpublished');
            $table->softDeletes();
            $table->unsignedSmallInteger('created_by')->default(1);
            $table->unsignedSmallInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opinions');
    }
}
