<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seat_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('seat_id');
            $table->foreign('seat_id')->references('id')->on('seats');
            $table->unsignedInteger('election_id');
            $table->foreign('election_id')->references('id')->on('elections');
            $table->unsignedInteger('division_id');
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->unsignedInteger('district_id');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->longText('extent')->nullable();
            $table->string('area')->nullable();
            $table->integer('male_voter')->nullable();
            $table->integer('female_voter')->nullable();
            $table->integer('total_voter')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->softDeletes();
            $table->unsignedSmallInteger('created_by')->default(1);
            $table->unsignedSmallInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seat_details');
    }
}
