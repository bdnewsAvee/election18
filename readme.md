<p align="center"><img src="https://adndigital.com.bd/wp-content/uploads/thegem-logos/logo_210e6c8637c66717a12464572a38f5f3_1x.png"></p>

## About BDNEWS24 Election Portal

This is a news portal for tracking election results and history. Also one of the main goal is graphical representation of those results and histories.
## Project Requirements
    Server Apache
    PHP >= 7.2
    OpenSSL PHP Extension
    PDO PHP Extension
    Mbstring PHP Extension
    Tokenizer PHP Extension
    XML PHP Extension
    Ctype PHP Extension
    JSON PHP Extension
    CURL
    MySql => 5.7
    
### Installation Process
Step 1 : Clone project


    git clone <repository-address>

Step 2 : Visit the project directory


    cd <project-name>

Step 3 : Install dependencies

    composer install 
Step 4 : Create environment file

    
    cp .env.example .env
Step 5 : Edit .env file as like below
    
       DB_CONNECTION=mysql
       DB_HOST=’host name’
       DB_PORT=’port’
       DB_DATABASE=’Database Name’
       DB_USERNAME=’Username’
       DB_PASSWORD=’Password’
Step 6 : Generate application key

    php artisan key:generate
Step 7 : Set write permission for ‘storage’ and ‘bootstrap/cache’ folder

Step 8 : Migrate database

    php artisan migrate


## Others
- Visit bangladeshMap.json file and change the value of <b>'link2ndlevel'</b> with production domain URL


     <project-name>/public/map/bangladeshMap.json

### Thanks and Enjoy !!